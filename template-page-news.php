<?php
/**
 * ===============================
 * TEMPLATE-PAGE-NEWS - template for news page
 * ===============================
 *
 * Template name: Aktualności
 *
 * @package PG
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>

    
    <main>
        <?php 
        get_template_part( 'template-parts/partial', 'news-list');
        ?>        
    </main>               
     

<?php
get_footer();