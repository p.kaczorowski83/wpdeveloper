
(function($) {

  $(document).ready(function(){
   
    const gallery = new Swiper('.hero', {
        loop: false,
        effect: "fade",
        autoplay: {
          delay: 3500,
          disableOnInteraction: false,
        },

      pagination: {
        el: '.swiper-pagination',
         dynamicBullets: false,
         clickable: true,
      },

      grabCursor: true,
    });


    const galleryBox = new Swiper('.investment-single-box-50-50 .gallBox, .standard-packages .cnt-loop .gallBox', {
        loop: false,
        effect: "fade",

        lazy: true,

      pagination: {
        el: '.swiper-pagination',
         clickable: true,
      },

      // Navigation arrows
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

      grabCursor: true,
    });


    const swiperLogo = new Swiper('.standard-producers-loga', {
        slidesPerView: "auto",
        slidesPerView: 1,
        spaceBetween: 0,

      // RWD
      breakpoints: {
    // when window width is >= 320px
    320: {
      slidesPerView: 2,
      spaceBetween: 20
    },
    // when window width is >= 480px
    480: {
      slidesPerView: 3,
      spaceBetween: 30
    },
    // when window width is >= 640px
    920: {
      slidesPerView: 5,
      spaceBetween: 30
    }
     }, 
      grabCursor: true,
    });


    const about = new Swiper('.about-creator-slider', {
        loop: true,
        effect: "fade",
        autoplay: false,

      // Navigation arrows
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

      grabCursor: true,
    });
});


  const swiperLogo = new Swiper('.home-investments-list .container', {
        slidesPerView: "auto",
        slidesPerView: 2,
        spaceBetween: 0,

        // Navigation arrows
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

      // RWD
      breakpoints: {
    // when window width is >= 320px
    320: {
      slidesPerView: 1,
      spaceBetween: 0
    },
    // when window width is >= 480px
    480: {
      slidesPerView: 1,
      spaceBetween: 20
    },
    // when window width is >= 640px
    920: {
      slidesPerView: 2,
      spaceBetween: 0
    }
     }, 
      grabCursor: true,
    });



}

(jQuery));