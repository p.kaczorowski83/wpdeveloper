$('.filters.maps select').bind('change', function(){
  var data = $('#search').serialize();
  var map = $(this).data('map');
  jQuery.ajax({
   type : "post",
   dataType : "json",
   url : ajax.url,
   data : {
     action: "search_on_maps",
     data: data,
   },
   beforeSend: function(response) {
     $('.loader').fadeIn();
   },
   success: function(response) {
      $('.loader').fadeOut();

      if(response.status == 'success'){
        var wyprawy = response.data;
        var items = [];
        var locations = [];
        var title,lat,lng,opis,cena,transport,kondycja,wolne_miejsca,wyprawa,link;
        $.each (wyprawy, function(index, itemData) {
          title 				= itemData.title;
          lat   				= itemData.lat;
          lng   				= itemData.lng;
          opis          = itemData.opis;
          cena          = itemData.cena;
          transport 	  = itemData.transport;
          kondycja  	  = itemData.kondycja;
          wolne_miejsca = itemData.wolne_miejsca;
          wyprawa   		= itemData.wyprawa;
          link   				= itemData.link;
          var items = [title, lat, lng, opis, cena, transport, kondycja, wolne_miejsca, wyprawa, link];
          locations.push(items)
        });

        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 52, lng: 21},
          zoom: 8
        });

        var oms = new OverlappingMarkerSpiderfier(map, {
          markersWontMove: true,
          markersWontHide: true,
          basicFormatEvents: true
        });

        var marker, count;
        var infowindow = new google.maps.InfoWindow();
        function iwClose() { infowindow.close(); }
        google.maps.event.addListener(map, 'click', iwClose);
        var bounds = new google.maps.LatLngBounds();
        for (count = 0; count < locations.length; count++) {
         (function() {  // make a closure over the marker and marker data
           var position    = new google.maps.LatLng(locations[count][1], locations[count][2]);
           var icon        = 'https://viaverde.com.pl/wp-content/themes/viaverde/img/man-cycling.png';
           var description = locations[count][3];
           var marker = new google.maps.Marker({
             position: position,
             title: locations[count][0],
             icon: icon,
             description: description,
             cena: locations[count][4],
             transport: locations[count][5],
             kondycja: locations[count][6],
             wolne_miejsca: locations[count][7],
             info: locations[count][8],
             url: locations[count][9],

           });
           google.maps.event.addListener(marker, 'spider_click', function(e) {  // 'spider_click', not plain 'click'
             infowindow.setContent(
               '<div class="infowindow">'+
                 '<div class="block-wyprawy">'+
                   '<div class="row">'+
                     '<div class="col-lg-12">'+
                       '<h2><a href="' + this.url + '">' + this.title + '</a></h2>'+
                       '<p>' + this.info + '</p>'+
                     '</div>'+
                   '</div>'+
                   '<div class="row">'+
                     '<div class="col-lg-6">'+
                       '<div class="meta">'+
                         '<p>'+
                           '<span class="label">cena:</span>'+
                           '<span class="value">' + this.cena + '</span>'+
                         '</p>'+
                         '<p>'+
                           '<span class="label">transport:</span><span class="value">' + this.transport + '</span>'+
                           '</p>'+
                         '<p>'+
                           '<span class="label">kondycja:</span>'+
                           '<span class="value"><span class="block-kondycja"><span style="width: ' + this.kondycja*21 + 'px;" class="result"></span></span></span>'+
                         '</p>'+
                         '<p>'+
                           '<span class="label">wolne miejsca:</span>'+
                           '<span class="value"> ' + this.wolne_miejsca + ' </span>'+
                         '</p>'+
                       '</div>'+
                     '</div>'+
                   '<div class="col-6 d-none d-lg-block">' + this.description + '</div>'+
                 '</div>'+
               '</div>'+
             '</div>'
             );
             infowindow.open(map, marker);
           });

           oms.addMarker(marker);  // adds the marker to the spiderfier _and_ the map

         bounds.extend(position)
         })();
        }

        if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
          var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.1, bounds.getNorthEast().lng() + 0.1);
          var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.1, bounds.getNorthEast().lng() - 0.1);
          bounds.extend(extendPoint1);
          bounds.extend(extendPoint2);
        }

        map.fitBounds(bounds);


      } else{
        alert("Brak wypraw spełniąjących kryteria");
      }

   }
 });
})
