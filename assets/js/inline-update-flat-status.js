jQuery(function ($) {

    $(document).on('change', '.inline-update-flat-status', function() {
        var value   = $(this).val();
        var post_id = $(this).data('post-id');

        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
                action: 'inline_update_flat_status',
                value: value,
                post_id : post_id,
            },

            success: function (response) {
                console.log('Poprawnie zmienione status apartamentu');
            }
        });
    })

});
