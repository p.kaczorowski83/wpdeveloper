<?php
/**
 * ===============================
 * TEMPLATE-PAGE-ABOUT - template for standard page
 * ===============================
 *
 * Template name: O nas
 *
 * @package PG
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>

    
    <main>
        <?php 
        get_template_part( 'template-parts/partial', 'lead');
        get_template_part( 'template-parts/partial', 'about-creator');
        get_template_part( 'template-parts/partial', 'about-foto-box');
        get_template_part( 'template-parts/partial', 'about-banner-quality');
        get_template_part( 'template-parts/partial', 'about-office');
        get_template_part( 'template-parts/partial', 'about-banner-comfort');
        ?>        
    </main>               
     

<?php
get_footer();