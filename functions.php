<?php
	
require get_template_directory() . '/inc/enqueue.php';
require get_template_directory() . '/inc/theme-support.php';
require get_template_directory() . '/inc/gutenberg-off.php';
require get_template_directory() . '/inc/custom-post.php';
require get_template_directory() . '/inc/options-page.php';
require get_template_directory() . '/inc/remove-jquery-migrate.php';
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';
require get_template_directory() . '/inc/apartments-list.php';
require get_template_directory() . '/inc/apartments-load-floor.php';


add_filter( 'manage_flat_posts_columns', 'set_custom_edit_flat_columns');
function set_custom_edit_flat_columns($columns) {
	unset($columns['date']);
    $columns['status'] = __( 'Status' );
	$columns['date'] = __( 'Date' );
    return $columns;
}


add_action( 'manage_flat_posts_custom_column' , 'custom_flat_column', 10, 2 );
function custom_flat_column( $column, $post_id ) {
    switch ( $column ) {
        case 'status' :
			echo '<select name="flat_status" data-post-id="'.$post_id.'" class="inline-update-flat-status">';
				$current_status = get_field('flat_status', $post_id);
				$statuses = array('wolne', 'rezerwacja', 'sprzedane');
				foreach ($statuses as $status){
					if($current_status == $status){
						echo '<option value="'.$status.'" selected>'.$status.'</option>';
					} else{
						echo '<option value="'.$status.'">'.$status.'</option>';
					}
				}
			echo '</select>';
        break;
    }
}

add_action( 'admin_enqueue_scripts', function() {
	wp_register_script('inline-update-flat-status', get_template_directory_uri() . '/assets/js/inline-update-flat-status.js', array('jquery'), '');
	wp_localize_script('inline-update-flat-status', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
	wp_enqueue_script('inline-update-flat-status');
} );



add_action("wp_ajax_inline_update_flat_status", "inline_update_flat_status");
function inline_update_flat_status(){
   
	$post_id    = (int)$_POST['post_id'];
	$new_status = $_POST['value'];

	if( update_field('flat_status', $new_status, $post_id) ){
		echo 'Poprawnie zmieniono status apratmanetu';
	} else{
		echo 'Nie zmieniono status apratmanetu';
	}

    wp_die();
}