<?php
/**
 * ===============================
 * TEMPLATE-PAGE-INVESTMENT- template for investment single page
 * ===============================
 *
 * Template name: Inwestycja
 *
 * @package PG
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>

    
    <main>
        <?php get_template_part( 'template-parts/partial', 'investment-single'); ?>    
    </main>               
     

<?php
get_footer();