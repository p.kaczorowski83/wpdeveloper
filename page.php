<?php
/**
 * ===============================
 * PAGE.PHP - The template for displaying default theme page
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>

<main class="main">
    <div class="container">
        
        <div class="main-cnt">
            <?php the_field( 'cnt' ); ?> 
        </div>

    </div><!-- edn /.container -->
</main>

<?php
get_footer();

