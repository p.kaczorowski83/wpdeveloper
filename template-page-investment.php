<?php
/**
 * ===============================
 * TEMPLATE-PAGE-INVESTMENT- template for investment page
 * ===============================
 *
 * Template name: Inwestycje
 *
 * @package PG
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>

    
    <main>
        <?php get_template_part( 'template-parts/partial', 'lead');
        get_template_part( 'template-parts/partial', 'investment-on-sale');
        get_template_part( 'template-parts/partial', 'investment-preparation');
        get_template_part( 'template-parts/partial', 'investment-sold'); ?>        
    </main>               
     

<?php
get_footer();