<?php
/**
 * ===============================
 * SINGLE PRODUCT.PHP - The template for displaying single post page
 * ===============================
 *
 * @package BEGO
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
get_template_part( 'template-parts/partial', 'product-hero');
get_template_part( 'template-parts/partial', 'breadcrumb');
?>

    <main class="main single-product">
        <div class="single-product-row">
            <div class="content">

                <div class="content-row">
                    <?php get_template_part( 'template-parts/partial', 'product-lead' );
                    get_template_part( 'template-parts/partial', 'product-info' );
                    get_template_part( 'template-parts/partial', 'product-quote' );
                    get_template_part( 'template-parts/partial', 'product-box' );
                    get_template_part( 'template-parts/partial', 'product-yellow-box' );
                    get_template_part( 'template-parts/partial', 'product-icon-box' );
                    get_template_part( 'template-parts/partial', 'product-security' );
                    get_template_part( 'template-parts/partial', 'product-composition' );
                    get_template_part( 'template-parts/partial', 'product-download' );
                    ?>
                </div>

            </div><!-- /. content -->

            <aside class="widget">
                <?php get_template_part( 'template-parts/partial', 'product-widget'); ?>
            </aside>

        </div><!-- ./ row -->
    </main>

<?php
get_template_part( 'template-parts/partial', 'banner-contact');
get_footer();

