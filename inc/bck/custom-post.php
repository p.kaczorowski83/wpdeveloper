<?php

// Register Custom Post Type
function custom_post_flat() {

  $labels = array(
    'name'                  => _x( 'Apartamenty', 'Post Type General Name', 'rg' ),
    'singular_name'         => _x( 'Apartament', 'Post Type Singular Name', 'rg' ),
    'menu_name'             => __( 'Apartamenty', 'rg' ),
    'name_admin_bar'        => __( 'Post Type', 'rg' ),
  );
  $args = array(
    'label'                 => __( 'Apartamenty', 'rg' ),
    'description'           => __( 'Post Type Description', 'rg' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor' ),
    'taxonomies'            => array( 'cat-flat' ),
    'hierarchical'          => true,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => false,
    'capability_type'       => 'page',
    'menu_icon'           => 'dashicons-admin-multisite',
    'rewrite' => array(
       'slug' => 'apartament'
     )
  );
  register_post_type( 'flat', $args );

}
add_action( 'init', 'custom_post_flat', 0 );

// Register Custom Taxonomy
function custom_flat() {

  $labels = array(
    'name'                       => _x( 'Inwestycja', 'Taxonomy General Name', 'rg' ),
    'singular_name'              => _x( 'Kategorie', 'Taxonomy Singular Name', 'rg' ),
    'menu_name'                  => __( 'Kategorie', 'rg' ),
    'all_items'                  => __( 'Wszystkie', 'rg' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'cat-flat', array( 'flat' ), $args );

  $labels = array(
    'name'                       => _x( 'Typ', 'Taxonomy General Name', 'rg' ),
    'singular_name'              => _x( 'Typ', 'Taxonomy Singular Name', 'rg' ),
    'menu_name'                  => __( 'Typ', 'rg' ),
    'all_items'                  => __( 'Typ', 'rg' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'cat-type', array( 'flat' ), $args );

  $labels = array(
    'name'                       => _x( 'Piętro', 'Taxonomy General Name', 'rg' ),
    'singular_name'              => _x( 'Piętro', 'Taxonomy Singular Name', 'rg' ),
    'menu_name'                  => __( 'Piętro', 'rg' ),
    'all_items'                  => __( 'Piętro', 'rg' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => false,
    //'public_query'               => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'cat-floor', array( 'flat' ), $args );

}
add_action( 'init', 'custom_flat', 0 );
