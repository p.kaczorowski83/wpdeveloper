<?php 

add_action("wp_ajax_load_floor", "load_floor");
add_action("wp_ajax_nopriv_load_floor", "load_floor");

function load_floor(){
    $floor_id = $_POST['floor_id'];
	$cat_flat = $_POST['cat_flat'];

	$floors = get_terms( 
        array( 
            'taxonomy' => 'cat-floor',
            'hide_empty' => false,
            'meta_query' => array(
                array(
                   'key'       => 'cat-flat',
                   'value'     => $cat_flat,
                )
            )
        )
    );

    ob_start();

	$floor_plan = get_field('floor_plan', 'term_'.$floor_id);


	?>
	<div class="navigation-3d">
		<a href="#" class="btn-blue back"><?php _e('Wróć', 'rg'); ?></a>

		<select name="floor" class="select2" data-cat-flat="<?php echo $cat_flat; ?>">
			<?php foreach ($floors as $floor) : ?>
				<option value="<?php echo $floor->term_id; ?>" <?php echo $floor->term_id == $floor_id ? 'selected' : '' ?>><?php echo $floor->description; ?></option>
			<?php endforeach ;?>
		</select>
	</div>
	
	<div class="search-map-floor search-map-floor-<?php echo $floor_id;?>" data-floor="<?php echo $floor_id;?>">
		<map name="map-floor-<?php echo $floor_id;?>" id="map-floor-<?php echo $floor_id;?>">
			<?php 
				$args = array(
					'post_type' => 'flat',
					'posts_per_page' => -1,
					'tax_query' => array(
						'relation'=> 'AND',
						array(
							'taxonomy' => 'cat-floor',
							'field'    => 'term_id',
							'terms'    => $floor_id,
						),
					),
				);

				$the_query = new WP_Query( $args ); ?>

				<?php if ( $the_query->have_posts() ) : ?>
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<?php if(get_field( 'flat_area' ) ) : ?>
							
						<?php 
							$status = get_field('flat_status');
							switch ($status) {
								case 'wolne':
									$bg = 'B7D5A9';
									break;
								case 'zarezerwowane':
									$bg = 'BDDBE7';
									break;
								case 'sprzedane':
									$bg = 'D5A9A9';
									break;
							} 	
						?>


						<area
							data-maphilight='{"alwaysOn":true,"fillOpacity":"0.65","fillColor":"<?php echo $bg; ?>","stroke": false}'
							target=""
							alt="
								<h3>Apartament <?php echo get_the_title() ; ?></h3>
								<p>Budynek: <?php echo get_field('flat_building')?></p>
								<p>Metraż: <?php echo get_field('flat_surface')?>m<sup>2</sup></p>
								<p>Typ: 
								<?php $terms = get_the_terms($post->ID, 'cat-type' );?>
									<?php if ($terms): ?>
										<?php foreach ($terms as $term) {echo "".$term_name = $term->name.'';}?>
									<?php endif ?>
								</p>
								<?php if( get_field( 'flat_surface_terrace')  ): ?>
									<p>Taras: <?php echo get_field( 'flat_surface_terrace'); ?> m<sup>2</sup></p>
								<?php endif;?>

								<?php if( get_field( 'flat_surface_balcony')  ): ?>
									<p>Balkon: <?php echo get_field( 'flat_surface_balcony'); ?> m<sup>2</sup></p>
								<?php endif;?>
								<p>Pokoje: <?php echo get_field('flat_room'); ?></p>"
							shape="poly"
							coords="<?php echo get_field('flat_area'); ?>" />
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; wp_reset_postdata(); ?>
		</map>

		<img id="image-floor-<?php echo $floor_id;?>" src="<?php echo $floor_plan ; ?>" alt="" usemap="#map-floor-<?php echo $floor_id;?>" />
	</div>
    
	<?php 

    $result['html'] = ob_get_contents();
    ob_end_clean();

    $result['status'] = true;

    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    }
    else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }

    die();
}

?>