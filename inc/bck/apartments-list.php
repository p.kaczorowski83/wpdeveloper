<?php
add_action("wp_ajax_apartments_filters", "apartments_filters");
add_action("wp_ajax_nopriv_apartments_filters", "apartments_filters");

function apartments_filters() {

  $form = $_POST['form'];
  parse_str($form, $output);

  $floors = (array)$output['floor'];

  if(isset($output['from']) && !empty($output['from'])){
    $from   = $output['from'];
    $meta['from'] = array(
      'key' => 'flat_surface',
      'value' => $from,
      'compare' => '>=',
      'type' => 'NUMERIC',
    );
  }

  if(isset($output['to']) && !empty($output['to'])){
    $to     = $output['to'];
    $meta['to'] = array(
      'key' => 'flat_surface',
      'value' => $to,
      'compare' => '<=',
      'type' => 'NUMERIC',
    );
  }

  if(isset($floors)){
    $meta['floor'] = array(
      'key' => 'flat_floor',
      'value' => $floors,
      'compare' => 'IN',
    );
  }

  if(isset($output['room'])){
    $rooms  = (array)$output['room'];
    $meta['room'] = array(
      'key' => 'flat_room',
      'value' => $rooms,
      'compare' => 'IN',
    );
  }

  if(isset($output['typ'])){
    $typ = (array)$output['typ'];
    $tax['typ'] = array(
        'taxonomy' => 'cat-type',
        'field'    => 'term_id',
        'terms'    => $typ
    );
  }


  $args = array(
    'post_type' => 'flat',
    'posts_per_page' => -1,
    'order' => 'ASC',
    'orderby' => 'title',
    'meta_query' => array(
      'relation'=> 'AND',
      $meta,
    ),
    'tax_query' => array(
     'relation'=> 'AND',
     $tax,
    ),
  );

  $the_query = new WP_Query( $args );


  ob_start();
  ?>



  <?php if ( $the_query->have_posts() ) : ?>
      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
          <?php get_template_part( 'template-parts/single', 'flat');?>
      <?php endwhile; ?>

  <?php else: ?>
    <p class="search-empty"><?php _e('Brak apartamentów o podanych parametrach', 'RG'); ?></p>
  <?php endif; wp_reset_postdata(); ?>




  <?php

  $result['list'] = ob_get_contents();
  ob_end_clean();


  if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      $result = json_encode($result);
      echo $result;
   }
   else {
      header("Location: ".$_SERVER["HTTP_REFERER"]);
   }

   die();

}

?>
