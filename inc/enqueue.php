<?php
/**
 * ===============================
 * LOADING FILES - CSS/JS
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
function pk_scripts_loader() {
	wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/main.css', array(), time());
	wp_enqueue_style( 'apartments', get_template_directory_uri() . '/assets/css/apartments.css', array(), time());
	wp_enqueue_style( 'select2', get_template_directory_uri() . '/assets/css/select2.min.css', array(), time());

	// SLIDER
	if( is_page( array(36,9,5,18,434,1) )) {
		wp_enqueue_script( 'swiperjs', get_template_directory_uri() . '/assets/js/swiper.js', array( 'jquery' ), time(), 'all');
		wp_enqueue_script( 'swipermainjs', get_template_directory_uri() . '/assets/js/swiper-main.js', array( 'jquery' ), time(), 'all');
		wp_enqueue_style( 'swiper', get_template_directory_uri() . '/assets/css/swiper.css', array(), time());
	}

	// FANCYBOX
	wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/assets/js/jquery.fancybox.js', array( 'jquery' ), time(), 'all');
	wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/assets/css/jquery.fancybox.min.css', array(), time());

	wp_enqueue_script( 'aos', get_template_directory_uri() . '/assets/js/aos.js', array( 'jquery' ), time(), 'all');
	wp_enqueue_script( 'maps', get_template_directory_uri() . '/assets/js/google-maps.js', array( 'jquery' ), time(), 'all');
	wp_enqueue_script( 'mainjs', get_template_directory_uri() . '/assets/js/main.bundle.js', array( 'jquery' ), time(), 'all');
	wp_enqueue_script( 'imagemapster', get_template_directory_uri() . '/assets/js/jquery.imagemapster.min.js', array( 'jquery' ), time(), 'all');
	wp_enqueue_script( 'select2', get_template_directory_uri() . '/assets/js/select2.min.js', array( 'jquery' ), time(), 'all');
	wp_enqueue_script( 'maphilight', get_template_directory_uri() . '/assets/js/jquery.maphilight.min.js', array( 'jquery' ), time(), 'all');
	wp_enqueue_script( 'mapresizer', get_template_directory_uri() . '/assets/js/mapresizer.min.js', array( 'jquery' ), time(), 'all');
	wp_enqueue_script( 'propper', 'https://unpkg.com/@popperjs/core@2', array( 'jquery' ), time(), 'all');
	wp_enqueue_script( 'tippy', 'https://unpkg.com/tippy.js@6', array( 'jquery' ), time(), 'all');
	

	wp_register_script('apartments', get_template_directory_uri() . '/assets/js/apartments.js', array('jquery'), '');
	wp_localize_script('apartments', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
	wp_enqueue_script('apartments');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', array(), time());

	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
	wp_dequeue_style( 'wc-block-style' );

}
add_action( 'wp_enqueue_scripts', 'pk_scripts_loader' );


if( !is_admin()){
      wp_deregister_script('jquery');
      wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"),
      false, '1.11.3', true);
      wp_enqueue_script('jquery');
 }


add_action('wp_default_scripts', 'remove_jquery_migrate');
