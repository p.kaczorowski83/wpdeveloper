<?php
/**
 * ===============================
 * TEMPLATE-PAGE-GALERU - template for galery page
 * ===============================
 *
 * Template name: Galeria
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>

    
    <main>
        <?php get_template_part( 'template-parts/partial', 'gallery-page' ); ?>        
    </main>               
     

<?php
get_footer();