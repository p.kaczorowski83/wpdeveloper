<?php
/**
 * ===============================
 * FOOTER.PHP - footer
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<footer id="footer">
    <?php 
        get_template_part( 'template-parts/partial', 'footer-col' );
        get_template_part( 'template-parts/partial', 'footer-copy' );
    ?>  
</footer><!-- /#footer -->

</div>


<?php 
    get_template_part( 'template-parts/partial', 'popup' );
    get_template_part( 'template-parts/partial', 'gallery' );
?>


<?php wp_footer(); ?>
<script>

function goBack() {
    window.history.back();
}

AOS.init({
    once: true,
    disable: 'mobile'
});
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC6Li_qKc_sbB4HETOUZKwGqPhAvpIZd4"></script>

</body>

</html>