<?php
/**
 * ===============================
 * TEMPLATE-PAGE-STANDARD - template for standard page
 * ===============================
 *
 * Template name: Standard
 *
 * @package PG
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>

    
    <main>
        <?php 
        get_template_part( 'template-parts/partial', 'lead');
        get_template_part( 'template-parts/partial', 'standard-foto-box');
        get_template_part( 'template-parts/partial', 'standard-producers');
        get_template_part( 'template-parts/partial', 'standard-finish');
        get_template_part( 'template-parts/partial', 'standard-banner');
        // get_template_part( 'template-parts/partial', 'standard-packages');
        get_template_part( 'template-parts/partial', 'standard-pack');
        get_template_part( 'template-parts/partial', 'standard-quality');
        ?>        
    </main>               
     

<?php
get_footer();