<?php
/**
 * ===============================
 * PARTIAL HOME BANNER.PHP 
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="banner">
    <?php $banner_img = get_field( 'home_banner_img' ); ?>
    <?php $size = 'full'; ?>
    <?php echo wp_get_attachment_image( $banner_img, $size, false, [
        'class' => 'lazyload img-fluid',
        'loading' => 'lazy',
        'data-src' => wp_get_attachment_image_url( $banner_img , $size ),
        'alt' => get_post_meta( $banner_img , '_wp_attachment_image_alt', true),
        ]); 
    ?>
    <?php $banner_subtitle = get_field( 'home_banner_subtitle' );
        $banner_title = get_field( 'home_banner_title' );
        $banner_cnt = get_field( 'home_banner_cnt' ); 
        $banner_link = get_field( 'home_banner_link' )
        ?>
    <div class="txt full">
        <?php if ($banner_subtitle): ?>
        <div class="subtitle" data-aos="fade-up">
            <?php echo $banner_subtitle; ?>
        </div>
        <?php endif ?>
        <?php if ($banner_title): ?>
        <div class="title-big" data-aos="fade-up">
            <?php echo $banner_title; ?>
        </div>
        <?php endif ?>
        <?php if ($banner_cnt): ?>
        <div data-aos="fade-up">
            <?php echo $banner_cnt ?>
        </div>
        <?php endif ?>
        <?php if ($banner_link): ?>
            <a href="<?php echo esc_url( $banner_link['url'] ); ?>" data-aos="fade-up"><?php echo esc_html( $banner_link['title'] ); ?></a>
        <?php endif; ?>
    </div>
</div>