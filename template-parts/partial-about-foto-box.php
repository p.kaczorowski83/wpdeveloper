<?php
/**
 * ===============================
 * ABOUT FOTO BOX.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
$about_lead_name = get_field( 'about_lead_name' );
?>

<?php if ($about_lead_name): ?>
<div class="about-photo">
    <div class="container">
        <h2 data-aos="fade-up">
            <?php echo $about_lead_name ?>
        </h2>
    </div>
</div>
<?php endif ?>

<div class="investment-single-two-photo">

        <div class="foto1">
            <?php $size = 'full'; ?>
            <?php $about_foto_left = get_field( 'about_foto_left' ); ?>
            <?php if ( $about_foto_left ) : ?>
                <?php echo wp_get_attachment_image( $about_foto_left, $size, false, [
                    'class' => 'lazyload img-fluid',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $about_foto_left , $size ),
                    'alt' => get_post_meta( $about_foto_left , '_wp_attachment_image_alt', true),
                    ]); 
                ?>
            <?php endif; ?>
        </div>
        <div class="cnt" data-aos="fade-up">
            <?php $about_foto_title = get_field( 'about_foto_title');
            $about_foto_cnt = get_field( 'about_foto_cnt' )
            ?>
            <?php if ($about_foto_title): ?>
                <div class="title-small">
                    <?php echo $about_foto_title; ?>
                </div>
            <?php endif ?>
            <?php echo $about_foto_cnt; ?>
        </div>
        <div class="foto2">
            <?php $about_foto_right = get_field( 'about_foto_right' ); ?>
            <?php $size = 'full'; ?>
            <?php if ( $about_foto_right ) : ?>
                <?php echo wp_get_attachment_image( $about_foto_right, $size, false, [
                    'class' => 'lazyload img-fluid',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $about_foto_right , $size ),
                    'alt' => get_post_meta( $about_foto_right , '_wp_attachment_image_alt', true),
                    ]); 
                ?>
            <?php endif; ?>
        </div>
    </div>