<!-- ================== [MENU MOBILE] ================== -->
<div class="mobile-overlay">
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>" class="logo-mm">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/logo.svg" alt="<?php bloginfo('name'); ?>" class="img-fluid">
	</a>	
	<nav><?php
        wp_nav_menu(
            array(
            'theme_location' => 'main-menu',
            'container'      => '',
            'menu_class'     => 'navbar__fixed-nav',
            'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
            'walker'         => new WP_Bootstrap_Navwalker(),
            )
        );
        ?>
	</nav>
</div>