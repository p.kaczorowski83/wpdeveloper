    <?php
/**
 * ===============================
 * HERO.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

$size = ''; 

?>

<?php if (have_rows( 'hero' )): ?>
<div class="hero swiper <?php if ( get_field( 'hero_height' ) == 1 ) : ?>hero-invest<?php endif; ?>">
    <ul class="swiper-wrapper">
        <?php while ( have_rows( 'hero' ) ) : the_row();
        $title = get_sub_field( 'hero_title' );
        $subtitle = get_sub_field( 'hero_subtitle' ) ?>
        <li class="swiper-slide">
            <?php $hero_img = get_sub_field( 'hero_img' ); ?>
            <?php if ( $hero_img ) : ?>
                <?php echo wp_get_attachment_image( $hero_img, $size, false, [
                    'class' => 'lazyload img-fluid',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $hero_img , $size ),
                    'alt' => get_post_meta( $hero_img , '_wp_attachment_image_alt', true),
                    ]); 
                ?>
            <?php endif; ?>
            <div class="hero-cnt">
                <div class="full">
                    <?php if ($subtitle): ?>
                        <div class="subtitle" data-aos="fade-up">
                            <?php echo $subtitle ?>
                        </div>    
                    <?php endif ?>
                    
                    <?php if ($title): ?>
                        <div data-aos="fade-up" class="title <?php if ( get_sub_field( 'hero_size' ) == 1 ) : ?>title-small<?php endif;?>">
                            <?php echo $title ?>
                        </div>
                    <?php endif ?>

                    <?php $hero_link = get_sub_field( 'hero_link' ); ?>
                    <?php if ( $hero_link ) : ?>
                        <?php if (is_page(36)): ?>
                            <a href="<?php echo esc_url( $hero_link['url'] ); ?>" target="_blank" class="btn-folder" data-aos="fade-up"><?php echo esc_html( $hero_link['title'] ); ?></a>
                        <?php else: ?>
                            <a href="<?php echo esc_url( $hero_link['url'] ); ?>" data-aos="fade-up"><span><?php echo esc_html( $hero_link['title'] ); ?></span></a>
                        <?php endif ?>
                        
                    <?php endif; ?>
                </div>
            </div>
        </li>
        <?php endwhile; ?>
    </ul>
    <div class="swiper-pagination"></div>
	
</div>
    
<?php endif ?>