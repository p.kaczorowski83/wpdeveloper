<?php
/**
 * ===============================
 * BANNER QUALITY QUALITY.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="banner">
    <?php $about_comfort_img = get_field( 'about_comfort_img' ); ?>
    <?php $size = 'full'; ?>
    <?php echo wp_get_attachment_image( $about_comfort_img, $size, false, [
        'class' => 'lazyload img-fluid',
        'loading' => 'lazy',
        'data-src' => wp_get_attachment_image_url( $about_comfort_img , $size ),
        'alt' => get_post_meta( $about_comfort_img , '_wp_attachment_image_alt', true),
        ]); 
    ?>
    <?php $about_comfort_subtitle = get_field( 'about_comfort_subtitle' );
        $about_comfort_title = get_field( 'about_comfort_title' );
        $about_comfort_cnt = get_field( 'about_comfort_cnt' ); 
        ?>
    <div class="txt full">
        <?php if ($about_comfort_subtitle): ?>
        <div class="subtitle" data-aos="fade-up">
            <?php echo $about_comfort_subtitle; ?>
        </div>
        <?php endif ?>
        <?php if ($about_comfort_title): ?>
        <div class="title-big" data-aos="fade-up">
            <?php echo $about_comfort_title; ?>
        </div>
        <?php endif ?>
        <?php if ($about_comfort_cnt): ?>
        <div data-aos="fade-up">
            <?php echo $about_comfort_cnt; ?>
        </div>
        <?php endif ?>
    </div>
</div>