<?php
/**
 * ===============================
 * FINANCING.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
$financing_lead = get_field('financing_lead');
?>

<div class="financing">
    <div class="text full">
        <?php if ($financing_lead): ?>
            <div class="lead" data-aos="fade-up">
                <?php echo $financing_lead ?>
            </div>
        <?php endif ?>

        <div class="person" data-aos="fade-up">
            <?php the_field( 'financing_person' ); ?>
        </div>
    </div>
    <div class="image" data-aos="fade-up">
        <?php $financing_foto = get_field( 'financing_foto' ); ?>
        <?php $size = 'full'; ?>
        <?php if ( $financing_foto ) : ?>
            <?php echo wp_get_attachment_image( $financing_foto, $size, false, [
                'class' => 'lazyload img-fluid',
                'loading' => 'lazy',
                'data-src' => wp_get_attachment_image_url( $financing_foto , $size ),
                'alt' => get_post_meta( $financing_foto , '_wp_attachment_image_alt', true),
                ]); 
            ?>
        <?php endif; ?>
    </div>

    <div class="form">
        <div class="container">
            <h2 class="typo1" data-aos="fade-up"><?php echo _e('Napisz do nas','rb');?></h2>
            <div data-aos="fade-up">
                <?php echo do_shortcode('[contact-form-7 id="186" title="Inwestycja"]'); ?>
            </div>
        </div>
    </div>
</div>