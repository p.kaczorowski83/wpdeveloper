<?php
/**
 * ===============================
 * CONTACT FORM.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="contact-form">
    <div class="container">
        <div class="title-small" data-aos="fade-up">
            <?php echo _e('Napisz do nas', 'rg'); ?>
        </div>
        <div data-aos="fade-up">
            <?php echo do_shortcode('[contact-form-7 id="186" title="Inwestycja"]'); ?>
        </div>
    </div>
</div>