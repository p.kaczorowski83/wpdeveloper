<?php
/**
 * ===============================
 * PARTIAL INVESTMENT BOX.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
$investment_box_header = get_field('investment_box_header');
?>

<section class="investment-lead">

    <!-- TITLE -->
    <?php if ($investment_box_header ): ?>
        <div class="container">
            <h2 class="typo1" data-aos="fade-up">
                <?php echo $investment_box_header;?>
            </h2>
        </div>        
    <?php endif ?>

    <!-- FOTO BOX -->
    <div class="investment-lead-box">
        <div class="foto-1" data-aos="fade-up">
            <?php $investment_box_foto_1 = get_field( 'investment_box_foto_1' ); ?>
            <?php $size = 'image413'; ?>
            <?php echo wp_get_attachment_image( $investment_box_foto_1, $size, false, [
                'class' => 'lazyload',
                'loading' => 'lazy',
                'data-src' => wp_get_attachment_image_url( $investment_box_foto_1 , $size ),
                'alt' => get_post_meta( $investment_box_foto_1 , '_wp_attachment_image_alt', true),
                ]); 
            ?>
        </div>
        <div class="text" data-aos="fade-up">
            <?php the_field( 'investment_box_cnt' ); ?>
        </div>
        <div class="foto-2" data-aos="fade-up">
            <?php $investment_box_foto_2 = get_field( 'investment_box_foto_2' ); ?>
            <?php $size = 'image810'; ?>
            <?php echo wp_get_attachment_image( $investment_box_foto_2, $size, false, [
                'class' => 'lazyload',
                'loading' => 'lazy',
                'data-src' => wp_get_attachment_image_url( $investment_box_foto_2 , $size ),
                'alt' => get_post_meta( $investment_box_foto_2 , '_wp_attachment_image_alt', true),
                ]); 
            ?>
        </div>
    </div>

</section>