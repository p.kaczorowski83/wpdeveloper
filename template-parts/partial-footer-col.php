<?php
/**
 * ===============================
 * FOOTER COL .PHP - footer copy
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<div class="container">
    <div class="cnt-col">
        <div class="col">
            <div class="address">
                <?php the_field( 'footer_sales', 'option' ); ?>
            </div>
            <div class="info">
                <?php the_field( 'footer_info', 'option' ); ?>
            </div>
        </div>
        <div class="col">
            <div class="address address-mobile">
                <?php the_field( 'footer_adress', 'option' ); ?>
            </div>
        </div>
        <div class="col">
            <h4>
                <?php echo _e('Inwestycje', 'rg') ;?>
            </h4>
            <?php
            wp_nav_menu(
                array(
                'theme_location' => 'inves-menu',
                )
            );
            ?>
        </div>
        <div class="col">
            <h4>
                <?php echo _e('Na skróty', 'rg') ;?>
            </h4>
            <?php
            wp_nav_menu(
                array(
                'theme_location' => 'footer-menu',
                )
            );
            ?>
        </div>
    </div>
</div>