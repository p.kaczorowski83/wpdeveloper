<?php
/**
 * ===============================
 * CONCATC SECRETARIAT.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
$contact_secretariat_title = get_field( 'contact_secretariat_title' );
?>

<div class="contact-secretariat">
    <div class="container">
        <?php if ($contact_secretariat_title): ?>
        <div class="row">
            <div class="title-small" data-aos="fade-up">
                 <?php echo $contact_secretariat_title ?>   
            </div>
        </div>
        <?php endif ?>
        <div class="row">
            <div class="col">
                <div class="name" data-aos="fade-up">
                    <?php the_field( 'contact_secretariat_name' ); ?>
                </div>
                <div class="position" data-aos="fade-up">
                    <?php the_field( 'contact_secretariat_position' ); ?>
                </div>
            </div>
            <div class="col" data-aos="fade-up">
                <div class="phone">
                    <a href="tel:<?php the_field( 'contact_secretariat_phone' ); ?>"><?php the_field( 'contact_secretariat_phone' ); ?></a>
                </div>
                <div class="mail">
                    <a href="mailto:<?php the_field( 'contact_secretariat_email' ); ?>"><?php the_field( 'contact_secretariat_email' ); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>