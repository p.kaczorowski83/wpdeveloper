<?php
/**
 * ===============================
 * LEAD.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
$standard_producers_subtitle = get_field( 'standard_producers_subtitle' );
$standard_producers_title = get_field( 'standard_producers_title' );
$standard_producers_cnt_left = get_field( 'standard_producers_cnt_left' );
$standard_producers_cnt_right = get_field( 'standard_producers_cnt_right');
?>

<section class="lead standard-producers">
    <div class="container">
        <?php if ($standard_producers_subtitle): ?>
            <h3 data-aos="fade-up">
                <?php echo $standard_producers_subtitle ?>
            </h3>
        <?php endif ?>
        <?php if ($standard_producers_title): ?>
            <h2 data-aos="fade-up">
                <?php echo $standard_producers_title ?>
            </h2>
        <?php endif ?>  
        <div class="row">
            <?php if ($standard_producers_cnt_right): ?>
            <div class="col" data-aos="fade-up">
                <?php echo $standard_producers_cnt_left ?>    
            </div>
            <div class="col" data-aos="fade-up">
                <?php echo $standard_producers_cnt_right ?>
            </div>
            <?php else: ?>
                <div data-aos="fade-up">
                    <?php echo $standard_producers_cnt_left ?>
                </div>  
            <?php endif ?>
        </div>

        <?php $standard_producers_loga_images = get_field( 'standard_producers_loga' ); ?>
        <?php if ( $standard_producers_loga_images ) :  ?>

        <div class="standard-producers-loga" data-aos="fade-up">
            <ul class="swiper-wrapper">
                <?php foreach ( $standard_producers_loga_images as $standard_producers_loga_image ): ?>
                <li class="swiper-slide">
                    <img src="<?php echo esc_url( $standard_producers_loga_image['url'] ); ?>" alt="<?php echo esc_attr( $standard_producers_loga_image['alt'] ); ?>" />
                </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <?php endif;?>
    </div>
</section>