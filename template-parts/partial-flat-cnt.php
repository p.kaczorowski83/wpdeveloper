<?php
/**
 * ===============================
 * PARTIAL FLAT CNT.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
$flat_floor = get_field('flat_floor');
$size = 'full';
?>
<?php $postid = get_the_ID(); ?>
<li class="flat" data-flat-id="<?php echo $postid;?>" data-flat-name="<?php the_title();?>" data-flat-surface="<?php the_field( 'flat_surface' ); ?>" data-flat-room="<?php the_field( 'flat_room' ); ?>" data-flat-floor="<?php echo get_field('flat_floor') == 0 ? 'Parter' : get_field('flat_floor'); ?>">
    <div class="flat-layer">
        <div class="flat-cnt flat-<?php echo $postid;?>">
            <div class="flat-data">
                <div class="flat-cnt">
                    <div class="img">
                        <?php $flat_img_floor = get_field( 'flat_img_floor' );
                        $flat_img = get_field( 'flat_img' );
                        $flat_surface_terrace = get_field('flat_surface_terrace');
                        $flat_surface_balcony = get_field('flat_surface_balcony');
                        $flat_pdf = get_field('flat_pdf');
                        ?>

                        <?php if ($flat_img_floor): ?>
                            <ul>
                                <li class="active level-01">
                                    <button>Poziom 1</button>
                                </li>
                                <li class="level-02">
                                    <button>Poziom 2</button>
                                </li>
                            </ul>
                            <div class="img-01">
                                 <?php echo wp_get_attachment_image( $flat_img, $size ); ?>
                            </div>
                            <div class="img-02">
                                <?php echo wp_get_attachment_image( $flat_img_floor, $size ); ?>
                            </div>
                        <?php else: ?>
                            <?php echo wp_get_attachment_image( $flat_img, $size ); ?>
                        <?php endif ?>
                    </div>
                    <div class="cnt">
                        <h3>Nr
                            <?php the_title();?>
                        </h3>
                        <ul>
                            <li>
                                Metraż:
                                <?php the_field( 'flat_surface' ); ?> m<sup>2</sup>
                            </li>
                            <li>
                                Liczba pokoi:
                                <?php the_field( 'flat_room' ); ?>
                            </li>
                            <li>
                                Piętro:
                                <?php $flat_floor = get_field('flat_floor'); if($flat_floor == "0") :?>
                                Parter
                                <?php else :?>
                                <?php echo $flat_floor;?>
                                <?php endif;?>
                            </li>
                            <?php if ($flat_surface_terrace): ?>
                            <li>
                                 Taras: <?php echo $flat_surface_terrace;?> m<sup>2</sup>
                            </li>
                            <?php endif ?>
                            <?php if ($flat_surface_balcony): ?>
                            <li>
                                 Balkon: <?php echo $flat_surface_balcony;?> m<sup>2</sup>
                            </li>
                            <?php endif ?>
                        </ul>
                        <button class="btn-base" data-offer-nr="<?php the_title();?>">Zapytaj o ofertę</button>
                        <a href="<?php echo esc_url( $flat_pdf['url'] ); ?>" class="btn flat-pdf" target="_blank">Pobierz kartę mieszkania</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</li>