<?php
/**
 * ===============================
 * PARTIAL FLAT FORM.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="flat-form">
    <div class="close">
        <svg xmlns="http://www.w3.org/2000/svg" width="26.417" height="26.413" viewBox="0 0 26.417 26.413">
        <path id="Union_16" data-name="Union 16" d="M-15793.793,14.621l-11.792,11.792L-15807,25l11.794-11.794L-15807,1.412l1.416-1.412,11.792,11.792L-15782,0l1.417,1.412-11.794,11.794L-15780.584,25l-1.417,1.412Z" transform="translate(15807.001)" fill="#c49b57"/>
        </svg>
    </div>
    <div class="container">
        <div class="cnt">
            <div class="row">
                <div class="txt">
                    <div class="flat-name"></div>
                    <div class="flat-room">Pokoje: <span></span></div>
                    <div class="flat-floor">Piętro: <span></span></div>
                    <div class="flat-surface">Powierzchnia: <span></span> m<sup>2</sup></div>

                    <div class="flat-tel">
                        <img width="52" height="52" src="/wp-content/uploads/2022/03/icon-tel.svg" class="attachment-full size-full" alt="">
                        tel. 730 094 777                        
                    </div>
                </div>
                <div class="form">
                    <h3>Zapytaj o ofertę</h3>
                    <?php echo do_shortcode('[contact-form-7 id="55" title="Stopka"]') ;?>
                </div>
            </div>
        </div>
    </div>
</div>

