<?php
/**
 * ===============================
 * STADNARD FOTO BOX.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="investment-single-two-photo">
        <div class="foto1">
            <?php $size = 'full'; ?>
            <?php $standard_foto_left = get_field( 'standard_foto_left' ); ?>
            <?php if ( $standard_foto_left ) : ?>
                <?php echo wp_get_attachment_image( $standard_foto_left, $size, false, [
                    'class' => 'lazyload img-fluid',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $standard_foto_left , $size ),
                    'alt' => get_post_meta( $standard_foto_left , '_wp_attachment_image_alt', true),
                    ]); 
                ?>
            <?php endif; ?>
        </div>
        <div class="cnt" data-aos="fade-up">
            <?php $standard_foto_title = get_field( 'standard_foto_title');
            $standard_foto_cnt = get_field( 'standard_foto_cnt' )
            ?>
            <?php if ($standard_foto_title): ?>
                <div class="title-small">
                    <?php echo $standard_foto_title; ?>
                </div>
                <?php echo $standard_foto_cnt; ?>
            <?php endif ?>
        </div>
        <div class="foto2">
            <?php $standard_foto_right = get_field( 'standard_foto_right' ); ?>
            <?php $size = 'full'; ?>
            <?php if ( $standard_foto_right ) : ?>
                <?php echo wp_get_attachment_image( $standard_foto_right, $size, false, [
                    'class' => 'lazyload img-fluid',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $standard_foto_right , $size ),
                    'alt' => get_post_meta( $standard_foto_right , '_wp_attachment_image_alt', true),
                    ]); 
                ?>
            <?php endif; ?>
        </div>
    </div>