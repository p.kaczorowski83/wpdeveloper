<?php
/**
 * ===============================
 * PARTIAL FLAT SEARCH.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<?php
$meta = array();
?>

<div class="c-123">
    <div class="loader"></div>

<form class="search-form" action="#" method="GET" data-aos="fade-up">
    <div class="container">
    <div class="row">
        <?php
        $setRooms = array();
        if(isset($_GET['room'])){
            $setRooms = (array)$_GET['room'];
        }
        $rooms = array(1,2,3);

        $types = get_terms( array(
            'taxonomy' => 'cat-type',
            'hide_empty' => false,
            ) 
        );
        $setFloors = array();
        if(isset($_GET['floor'])){
            $setFloors = (array)$_GET['floor'];
        }        
        $floors = array(0,1,2);
        ?>
        <div class="room-col">
            <p><strong><?php _e('Ilość pokoi', 'rg'); ?></strong></p>
            <div class="field field__select">
                <select name="room[]" class="select2" multiple="multiple">
                    <option></option>
                    <?php foreach ($rooms as $room) :?>
                        <option value="<?php echo $room; ?>"><?php echo $room; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <div class="surface-col">
            <p><strong><?php _e('Powierzchnia', 'rg'); ?></strong></p>
            <div class="field field__text">
                <label>
                    <input type="text" name="from" value="" autocomplete="off" placeholder="<?php _e('Od', 'rg'); ?>">
                </label>
                <label>
                    <input type="text" name="to" value="" autocomplete="off" placeholder="<?php _e('Do', 'rg'); ?>">
                </label>
            </div>
        </div>

        <div class="type-col">
            <p><strong><?php _e('Typ', 'rg'); ?></strong></p>
            <div class="field field__select">
                <select name="typ[]" class="select2" multiple="multiple">
                    <option></option>
                    <?php if( $types ) : ?>
                        <?php foreach( $types as $type ) : ?>
                                <option value="<?php echo $type->term_id; ?>"><?php echo $type->name; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
         </div>

    <div class="floor-col">
        <p><strong><?php _e('Piętro', 'rg'); ?></strong></p>
        <div class="field field__select">
            <select name="floor[]" class="select2" multiple="multiple">
                <option></option>
                <?php foreach ($floors as $floor) :?>
                    <option value="<?php echo $floor; ?>"><?php echo $floor; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <div class="button-col col__button">
        <button id="resetBtn"><?php _e('Wyczyść filtry', 'rg'); ?></button>
    </div>

</div>
</div>
</form>



<div class="search-list" data-aos="fade-up">
    <div class="container">
        <ul class="search-table-header">
            <li>
                Numer
            </li>
            <li>
                Piętro
            </li>
            <li>
                Pokoje
            </li>
            <li>
                Powierzchnia
            </li>
            <li>
                Typ
            </li>
            <li>
                Cena
            </li>
            <li>
                Status
            </li>
            <li>
                Karta mieszkania
            </li>
        </ul>

        <div class="search-table-list">
            <?php
            if(isset($_GET['from']) && !empty($_GET['from'])) {
                $meta['from'] = array(
                'key' => 'flat_surface',
                'value' => $_GET['from'],
                'compare' => '>=',
                'type' => 'NUMERIC',
                );
            }
            if(isset($_GET['to']) && !empty($_GET['to'])){
            $meta['to'] = array(
            'key' => 'flat_surface',
            'value' => $_GET['to'],
            'compare' => '<=',
            'type' => 'NUMERIC',
            );
            }
            if(isset($_GET['floor'])){
                $meta['floor'] = array(
                    'key' => 'flat_floor',
                    'value' => $setFloors,
                    'compare' => 'IN',
                );
            }
            if(isset($_GET['room'])){
                $meta['room'] = array(
                    'key' => 'flat_room',
                    'value' => $setRooms,
                    'compare' => 'IN',
              );
            }

            $invest_single_search_cat = get_sub_field( 'invest_single_search_cat' );

            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

            $args = array(
            'post_type' => 'flat',
            'posts_per_page' => 80,
            'paged' => $paged,
            'order' => 'ASC',
            'orderby' => 'title',
            'tax_query' => array(
                array(
                    'taxonomy' => 'cat-flat', //double check your taxonomy name in you dd 
                    'field'    => 'id',
                    'terms'    => $invest_single_search_cat,
                ),
               ),
            'meta_query' => array(
            'relation'=> 'AND',
                $meta,
            )
            );
            $the_query = new WP_Query( $args );
            ?>

            <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <?php get_template_part( 'template-parts/single', 'flat');?>
                <?php endwhile; ?>
            <?php else: ?>
                <p class="text-center"><?php _e('Brak apartamentów o podanych parametrach', 'rg'); ?></p>
            <?php endif; wp_reset_postdata(); ?>

        </div><!-- end .search-list -->
    </div><!-- end .container -->
</div><!-- end .search-list -->
</div><!-- end c-123 -->