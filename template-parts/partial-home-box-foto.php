<?php
/**
 * ===============================
 * PARTIAL HOME BOX FOTO.PHP 
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="home-box">
    <div class="row">
        <div class="col full">
            <div class="home-box-txt">
                <?php $home_box_subtitle = get_field('home_box_subtitle');
                $home_box_title = get_field('home_box_title');
                $home_box_cnt = get_field('home_box_cnt'); ?>
                <?php if ($home_box_subtitle): ?>
                    <h3 data-aos="fade-up">
                        <?php echo $home_box_subtitle ?>
                    </h3>
                <?php endif ?>
                <?php if ($home_box_title): ?>
                    <h2 data-aos="fade-up">
                        <?php echo $home_box_title ?>
                    </h2>
                <?php endif ?>
                <?php if ($home_box_cnt): ?>
                    <div data-aos="fade-up">
                        <?php echo $home_box_cnt ?>
                    </div>
                <?php endif ?>

                <?php $home_box_link = get_field( 'home_box_link' ); ?>
                <?php if ( $home_box_link ) : ?>
                    <div data-aos="fade-up">
                        <a href="<?php echo esc_url( $home_box_link['url'] ); ?>" class="btn-blue"><?php echo esc_html( $home_box_link['title'] ); ?></a>
                    </div>
                <?php endif; ?>

            </div>
        </div>
        <div class="col">
            <?php $home_box_img = get_field( 'home_box_img' ); ?>
            <?php $size = 'full'; ?>
            <?php if ( $home_box_img ) : ?>
                <?php echo wp_get_attachment_image( $home_box_img, $size, false, [
                    'class' => 'lazyload img-fluid',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $home_box_img , $size ),
                    'alt' => get_post_meta( $home_box_img , '_wp_attachment_image_alt', true),
                    'data-aos' => "fade-up"
                    ]); 
                ?>
            <?php endif; ?>
        </div>
    </div>
</div>