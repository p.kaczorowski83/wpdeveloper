<?php
/**
 * ===============================
 * PARTIAL INVESTMENT SOLUTIONS.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
$investment_solutions_title = get_field( 'investment_solutions_title' );
$investment_solutions_lead = get_field( 'investment_solutions_lead' );
?>

<section class="investment-solutions blue-box">
    
    <div class="container">
        <?php if ( $investment_solutions_title ): ?>
            <h3 class="typo1" data-aos="fade-up">
                <?php echo $investment_solutions_title; ?>
            </h3>
        <?php endif; ?>
        <?php if ($investment_solutions_lead): ?>
            <div data-aos="fade-up">
                <?php echo $investment_solutions_lead;?>
            </div>
        <?php endif ?>

        <?php if ( have_rows( 'investment_solutions_loop' ) ) : ?>
            <ul class="list">
                <?php while ( have_rows( 'investment_solutions_loop' ) ) : the_row(); ?>
                    <li>
                        <?php $investment_solutions_loop_img = get_sub_field( 'investment_solutions_loop_img' ); ?>
                        <?php $size = 'full'; ?>
                        <?php if ( $investment_solutions_loop_img ) : ?>
                        <div class="icon" data-aos="fade-up">
                            <?php echo wp_get_attachment_image( $investment_solutions_loop_img, $size, false, [
                                'class' => 'lazyload',
                                'loading' => 'lazy',
                                'data-src' => wp_get_attachment_image_url( $investment_solutions_loop_img , $size ),
                                'alt' => get_post_meta( $investment_solutions_loop_img , '_wp_attachment_image_alt', true),
                                ]); 
                            ?>
                        </div>
                        <?php endif; ?>
                        <p data-aos="fade-up"><?php the_sub_field( 'investment_solutions_loop_txt' ); ?></p>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>

    </div>

</section>