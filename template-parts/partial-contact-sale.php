<?php
/**
 * ===============================
 * CONTACT SALE.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="contact-sale">
    <div class="container">
        <div class="row">
            <div class="title-small" data-aos="fade-up">
                 <?php the_field( 'contact_sale_title' ); ?>
            </div>
        </div>
        <?php if ( have_rows( 'contact_sale_loop' ) ) : ?>
            <ul class="row">
                <?php while ( have_rows( 'contact_sale_loop' ) ) : the_row(); ?>
                    <li class="col">
                        <div class="name" data-aos="fade-up">
                            <?php the_sub_field( 'contact_sale_loop_name' ); ?>
                        </div>
                        <div class="position" data-aos="fade-up">
                            <?php the_sub_field( 'contact_sale_loop_position' ); ?>
                        </div>
                        <div class="info" data-aos="fade-up">
                            <?php the_sub_field( 'contact_sale_loop_contact' ); ?>
                        </div>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>