<?php
/**
 * ===============================
 * CONTACT BOX.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="contact-box">
    <div class="container">
        <?php if ( have_rows( 'contact_loop' ) ) : ?>
        <div class="row">
            <?php while ( have_rows( 'contact_loop' ) ) : the_row(); ?>
                <div class="col">
                    <div class="title-small" data-aos="fade-up">
                        <?php the_sub_field( 'contact_loop_title' ); ?>
                    </div>
                    <div class="name" data-aos="fade-up">
                        <?php the_sub_field( 'contact_loop_name' ); ?>
                    </div>
                    <div class="position" data-aos="fade-up">
                        <?php the_sub_field( 'contact_loop_position' ); ?>
                    </div>
                    <div class="info" data-aos="fade-up">
                        <?php the_sub_field( 'contact_loop_contact' ); ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</div>