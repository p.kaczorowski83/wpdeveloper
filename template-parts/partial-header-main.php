<?php
/**
 * ===============================
 * HEADER-MAIN.PHP - menu and submenu
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="navbar__fixed-main">
    <!-- MENU  -->
    <nav aria-label="Main">
        <?php
        wp_nav_menu(
            array(
            'theme_location' => 'main-menu',
            'container'      => '',
            'menu_class'     => 'navbar__fixed-nav',
            'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
            'walker'         => new WP_Bootstrap_Navwalker(),
            )
        );
        ?>
    </nav>   
</div>

<div class="navbar__fixed-icon">
    <a href="tel:+48603881177">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/svg/icon-tel.svg" alt="<?php bloginfo('name'); ?>" width="21" height="21" class="img-fluid">
    </a>
    <div class="contact">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/svg/icon-mail.svg" alt="<?php bloginfo('name'); ?>" width="24" height="19" class="img-fluid">
    </div>
</div>