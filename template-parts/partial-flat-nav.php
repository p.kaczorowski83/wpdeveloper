<?php
/**
 * ===============================
 * PARTIAL FLAT NAV.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="flat-navigation">
  <ul>
    <li>
      <button class="btn-back"><svg xmlns="http://www.w3.org/2000/svg" width="14.989" height="11.98" viewBox="0 0 14.989 11.98"><path id="Union_1" data-name="Union 1" d="M8.01.99l4.3,4.3H0v1.4H12.311l-4.3,4.3.99.99L14.989,5.99,14,5h0L9,0Z" transform="translate(14.989 11.98) rotate(180)" fill="#fff"/></svg> Powrót</button>
    </li>
    <li>
      <select class="select-floor">
        <option value="#apartament/0" id="floor0">Parter</option>
        <option value="#apartament/1" id="floor1">Piętro 1</option>
        <option value="#apartament/2" id="floor2">Piętro 2</option>
        <option value="#apartament/3" id="floor3">Piętro 3</option>
      </select>
    </li>
  </ul>
</div>