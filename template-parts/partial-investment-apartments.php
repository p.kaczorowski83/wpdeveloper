<?php
/**
 * ===============================
 * PARTIAL INVESTMENT APARTMENTS.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
$investment_apartments_title = get_field('investment_apartments_title');
?>

<div class="row">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/img-waves-investment.svg" alt="" class="img-fluid img-waves">
</div>

<section class="investment-apartments">

    <div class="container">
        <?php if ($investment_apartments_title): ?>
            <h2 class="typo1" data-aos="fade-up">
                <?php echo $investment_apartments_title; ?>
            </h2>
        <?php endif ?>
    </div>

        <?php $i=0; if ( have_rows( 'investment_apartments_loop' ) ) : ?>
            <?php while ( have_rows( 'investment_apartments_loop' ) ) : the_row(); $i++ ?>
                <div class="container">
                    <div class="investment-apartments-row">
                        <div class="name" data-aos="fade-right">
                            <?php the_sub_field( 'investment_apartments_loop_name' ); ?>
                        </div>
                        <div class="text" data-aos="fade-left">
                            <?php the_sub_field( 'investment_apartments_loop_cnt' ); ?>
                        </div>
                    </div>
                </div>                    

                <?php $investment_apartments_loop_gallery_images = get_sub_field( 'investment_apartments_loop_gallery' ); ?>
                <?php if ( $investment_apartments_loop_gallery_images ) :  ?>
                <aside class="home-functional-gallery invest-gallery gallery-<?php echo $i?>">
            
                <div class="swiper">
                    <div class="swiper-wrapper">
                        <?php foreach ( $investment_apartments_loop_gallery_images as $investment_apartments_loop_gallery_image ): ?>
                        <div class="swiper-slide">
                            <a href="<?php echo esc_url( $investment_apartments_loop_gallery_image['url'] ); ?>" data-fancybox="gallery">
                                <img src="<?php echo esc_url( $investment_apartments_loop_gallery_image['sizes']['image856'] ); ?>" class="img-fluid" alt="<?php echo esc_attr( $investment_apartments_loop_gallery_image['alt'] ); ?>" />
                            </a>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- <div class="swiper-pagination"></div>-->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
                </aside>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>