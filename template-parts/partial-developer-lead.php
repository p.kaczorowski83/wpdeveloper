<?php
/**
 * ===============================
 * PARTIAL DEVELOPER LEAD.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
$developer_lead = get_field( 'developer_lead' );
?>

<div class="developer-lead container" id="next">

    <div class="row">
        <div class="col">
            <?php if ( $developer_lead ): ?>
                <h2 class="typo1" data-aos="fade-up">
                    <?php echo $developer_lead; ?>
                </h2>
                <div data-aos="fade-up"><?php the_field( 'developer_lead_cnt' ); ?></div>
            <?php endif ?>
        </div>
        <div class="col">
            <?php $developer_lead_logo = get_field( 'developer_lead_logo' ); ?>
            <?php $size = 'full'; ?>
            <?php if ( $developer_lead_logo ) : ?>
                <?php echo wp_get_attachment_image( $developer_lead_logo, $size, false, [
                    'class' => 'lazyload',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $developer_lead_logo , $size ),
                    'alt' => get_post_meta( $developer_lead_logo , '_wp_attachment_image_alt', true),
                    'data-aos' => "fade-up"
                    ]); 
                ?>
            <?php endif; ?>
        </div>
    </div>

</div>