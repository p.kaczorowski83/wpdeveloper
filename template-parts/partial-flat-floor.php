<?php
/**
 * ===============================
 * PARTIAL BUILD.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<ul class="floor-list">
    <li class="floor" data-floor-id="0">
        <div class="floor-layer">
            <div class="floor-cnt">
               <img src="/wp-content/uploads/2022/04/img-mapa-parter-1.jpeg" class="mapster-floor-image" usemap="#floor-0-flats-map" style="width: 100%; height:auto; opacity: 1;">
               <map name="floor-0-flats-map">
                <?php 
                $posts = get_posts(array(
                                                'post_type' => 'flat',
                                                'posts_per_page' => 100
                                             ));
                                             foreach($posts as $post): 
                                             setup_postdata($post);
                                             $area = get_field('flat_area');
                                             $flat_floor = get_field('flat_floor');
                                             $postid = get_the_ID();
                                             $flat_status = get_field('flat_status');
                                             $flat_status = get_field('flat_status');
                                             $flat_surface_terrace = get_field('flat_surface_terrace');
                                             ?>
                                             <?php if ($flat_status == "wolne"): ?>
                                                <?php $flat = 1;?>
                                             <?php elseif ($flat_status == "zarezerwowane") :?>
                                                 <?php $flat = 2;?>
                                             <?php elseif ($flat_status == "sprzedane") :?>
                                                 <?php $flat = 3;?>
                                             <?php endif ?>
                                             <?php if ($flat_floor == '0'):?>
                                             <?php if ($area): ?>                                       
                                                <area data-flat="<?php echo $postid;?>" href="<?php if ($flat_status == "wolne"): ?>#apartament/0/<?php echo $postid;?><?php else :?>#<?php endif;?>" shape="poly" data-type="<?php echo $flat;?>" data-status="<?php echo $flat;?>" data-status-name="<?php the_field( 'flat_status' ); ?>" data-parameters-title="<?php the_title();?>" data-parameters="{&quot;Status&quot;:&quot;<?php the_field( 'flat_status' ); ?>&quot;,&quot;Powierzchnia&quot;:&quot;<?php the_field( 'flat_surface' ); ?> m<sup>2</sup>&quot;,&quot;Pokoje&quot;: &quot;<?php the_field( 'flat_room' ); ?>&quot;, &quot;Piętro&quot;: &quot;Parter&quot; <?php if ($flat_surface_terrace): ?>,&quot;Taras&quot;: &quot;<?php the_field( 'flat_surface_terrace' ); ?> m<sup>2</sup>&quot;<?php endif;?> ,&quot;Balkon&quot;: &quot;<?php the_field( 'flat_surface_balcony' ); ?> m<sup>2</sup>&quot;}" coords="<?php echo $area; ?>">
                                             <? endif; endif; wp_reset_postdata(); endforeach?>                                             
                                             
                                          </map>
                                       </div>

                                          <ul class="flat-list">
                                             <?php 
                                             $posts = get_posts(array(
                                                'post_type' => 'flat',
                                                'posts_per_page' => 100
                                             ));
                                             foreach($posts as $post): 
                                             setup_postdata($post);
                                             $area = get_field('flat_area');
                                             $flat_floor = get_field('flat_floor');
                                             $postid = get_the_ID();
                                             $flat_status = get_field('flat_status');
                                             $flat_pdf = get_field( 'flat_pdf' );
                                             ?>
                                             <?php if ($flat_status == "wolne" and $flat_floor == '0'): ?>
                                             <?php
                                                get_template_part( 'template-parts/partial', 'flat-cnt');
                                             ?>
                                          <? endif; wp_reset_postdata(); endforeach?>    
                                       </ul>
                                 </div>
                              </li>

   <li class="floor" data-floor-id="1">
                           <div class="floor-layer">
                              <div class="floor-cnt">
                                 <img src="/wp-content/uploads/2022/04/img-mapa-pietro-01.jpg" class="mapster-floor-image" usemap="#floor-1-flats-map" style="width: 100%; height:auto; opacity: 1;">
                                
                                          <map name="floor-1-flats-map">
                                             <?php 
                                             $posts = get_posts(array(
                                                'post_type' => 'flat',
                                                'posts_per_page' => 100
                                             ));
                                             foreach($posts as $post): 
                                             setup_postdata($post);
                                             $area = get_field('flat_area');
                                             $flat_floor = get_field('flat_floor');
                                             $postid = get_the_ID();
                                             $flat_status = get_field('flat_status');
                                             $flat_surface_terrace = get_field('flat_surface_terrace');
                                             ?>
                                             <?php if ($flat_status == "wolne"): ?>
                                                <?php $flat = 1;?>
                                             <?php elseif ($flat_status == "zarezerwowane") :?>
                                                 <?php $flat = 2;?>
                                             <?php elseif ($flat_status == "sprzedane") :?>
                                                 <?php $flat = 3;?>
                                             <?php endif ?>
                                             <?php if ($flat_floor == '1'):?>
                                             <?php if ($area): ?>                                       
                                                <area data-flat="<?php echo $postid;?>" href="<?php if ($flat_status == "wolne"): ?>#apartament/1/<?php echo $postid;?><?php else :?>#<?php endif;?>" shape="poly" data-type="<?php echo $flat;?>" data-status="<?php echo $flat;?>" data-status-name="<?php the_field( 'flat_status' ); ?>" data-parameters-title="<?php the_title();?>" data-parameters="{&quot;Status&quot;:&quot;<?php the_field( 'flat_status' ); ?>&quot;,&quot;Powierzchnia&quot;:&quot;<?php the_field( 'flat_surface' ); ?> m<sup>2</sup>&quot;,&quot;Pokoje&quot;: &quot;<?php the_field( 'flat_room' ); ?>&quot;, &quot;Piętro&quot;: &quot;1&quot; <?php if ($flat_surface_terrace): ?>,&quot;Taras&quot;: &quot;<?php the_field( 'flat_surface_terrace' ); ?> m<sup>2</sup>&quot;<?php endif;?> ,&quot;Balkon&quot;: &quot;<?php the_field( 'flat_surface_balcony' ); ?> m<sup>2</sup>&quot;}" coords="<?php echo $area; ?>">                                  
                                              <? endif; endif; wp_reset_postdata(); endforeach?>    
                                          </map>
                                       </div>

                                          <ul class="flat-list">
                                             <?php 
                                             $posts = get_posts(array(
                                                'post_type' => 'flat',
                                                'posts_per_page' => 100
                                             ));
                                             foreach($posts as $post): 
                                             setup_postdata($post);
                                             $area = get_field('flat_area');
                                             $flat_floor = get_field('flat_floor');
                                             $postid = get_the_ID();
                                             $flat_status = get_field('flat_status');
                                             $flat_pdf = get_field( 'flat_pdf' );
                                             ?>
                                             <?php if ($flat_status == "wolne" and $flat_floor == '1'): ?>
                                             <?php
                                                get_template_part( 'template-parts/partial', 'flat-cnt');
                                             ?>
                                          <? endif; wp_reset_postdata(); endforeach?>    
                                       </ul>
      </div>
   </li>


   <li class="floor" data-floor-id="2">
                           <div class="floor-layer">
                              <div class="floor-cnt">
                                 <img src="/wp-content/uploads/2022/04/img-mapa-pietro-2.jpg" class="mapster-floor-image" usemap="#floor-2-flats-map" style="width: 100%; height:auto; opacity: 1;">
                                 
                                          <map name="floor-2-flats-map">
                                             <?php 
                                             $posts = get_posts(array(
                                                'post_type' => 'flat',
                                                'posts_per_page' => 100
                                             ));
                                             foreach($posts as $post): 
                                             setup_postdata($post);
                                             $area = get_field('flat_area');
                                             $flat_floor = get_field('flat_floor');
                                             $postid = get_the_ID();
                                             $flat_status = get_field('flat_status');
                                             $flat_surface_terrace = get_field('flat_surface_terrace');
                                             ?>
                                             <?php if ($flat_status == "wolne"): ?>
                                                <?php $flat = 1;?>
                                             <?php elseif ($flat_status == "zarezerwowane") :?>
                                                 <?php $flat = 2;?>
                                             <?php elseif ($flat_status == "sprzedane") :?>
                                                 <?php $flat = 3;?>
                                             <?php endif ?>
                                             <?php if ($flat_floor == '2'):?>
                                             <?php if ($area): ?>                                       
                                                <area data-flat="<?php echo $postid;?>" href="<?php if ($flat_status == "wolne"): ?>#apartament/2/<?php echo $postid;?><?php else :?>#<?php endif;?>" shape="poly" data-type="<?php echo $flat;?>" data-status="<?php echo $flat;?>" data-status-name="<?php the_field( 'flat_status' ); ?>" data-parameters-title="<?php the_title();?>" data-parameters="{&quot;Status&quot;:&quot;<?php the_field( 'flat_status' ); ?>&quot;,&quot;Powierzchnia&quot;:&quot;<?php the_field( 'flat_surface' ); ?> m<sup>2</sup>&quot;,&quot;Pokoje&quot;: &quot;<?php the_field( 'flat_room' ); ?>&quot;, &quot;Piętro&quot;: &quot;2&quot; <?php if ($flat_surface_terrace): ?>,&quot;Taras&quot;: &quot;<?php the_field( 'flat_surface_terrace' ); ?> m<sup>2</sup>&quot;<?php endif;?> ,&quot;Balkon&quot;: &quot;<?php the_field( 'flat_surface_balcony' ); ?> m<sup>2</sup>&quot;}" coords="<?php echo $area; ?>">
                                             <? endif; endif; wp_reset_postdata(); endforeach?>                                             
                                             
                                          </map>
                                       </div>

                                          <ul class="flat-list">
                                             <?php 
                                             $posts = get_posts(array(
                                                'post_type' => 'flat',
                                                'posts_per_page' => 100
                                             ));
                                             foreach($posts as $post): 
                                             setup_postdata($post);
                                             $area = get_field('flat_area');
                                             $flat_floor = get_field('flat_floor');
                                             $postid = get_the_ID();
                                             $flat_status = get_field('flat_status');
                                             $flat_pdf = get_field( 'flat_pdf' );
                                             ?>
                                             <?php if ($flat_status == "wolne" and $flat_floor == '2'): ?>
                                             <?php
                                                get_template_part( 'template-parts/partial', 'flat-cnt');
                                             ?>
                                          <? endif; wp_reset_postdata(); endforeach?>    
                                       </ul>
      </div>
   </li>
   <li class="floor" data-floor-id="3">
                           <div class="floor-layer">
                              <div class="floor-cnt">
                                 <img src="/wp-content/uploads/2022/04/img-mapa-pietro-03.jpg" class="mapster-floor-image" usemap="#floor-3-flats-map" style="width: 100%; height:auto; opacity: 1;">
                                          <map name="floor-3-flats-map">
                                             <?php 
                                             $posts = get_posts(array(
                                                'post_type' => 'flat',
                                                'posts_per_page' => 100
                                             ));
                                             foreach($posts as $post): 
                                             setup_postdata($post);
                                             $area = get_field('flat_area');
                                             $flat_floor = get_field('flat_floor');
                                             $postid = get_the_ID();
                                             $flat_status = get_field('flat_status');
                                             $flat_surface_terrace = get_field('flat_surface_terrace');
                                             ?>
                                             <?php if ($flat_status == "wolne"): ?>
                                                <?php $flat = 1;?>
                                             <?php elseif ($flat_status == "zarezerwowane") :?>
                                                 <?php $flat = 2;?>
                                             <?php elseif ($flat_status == "sprzedane") :?>
                                                 <?php $flat = 3;?>
                                             <?php endif ?>
                                             <?php if ($flat_floor == '3'):?>
                                             <?php if ($area): ?>                                       
                                                <area data-flat="<?php echo $postid;?>" href="<?php if ($flat_status == "wolne"): ?>#apartament/3/<?php echo $postid;?><?php else :?>#<?php endif;?>" shape="poly" data-type="<?php echo $flat;?>" data-status="<?php echo $flat;?>" data-status-name="<?php the_field( 'flat_status' ); ?>" data-parameters-title="<?php the_title();?>" data-parameters="{&quot;Status&quot;:&quot;<?php the_field( 'flat_status' ); ?>&quot;,&quot;Powierzchnia&quot;:&quot;<?php the_field( 'flat_surface' ); ?> m<sup>2</sup>&quot;,&quot;Pokoje&quot;: &quot;<?php the_field( 'flat_room' ); ?>&quot;, &quot;Piętro&quot;: &quot;3&quot; <?php if ($flat_surface_terrace): ?>,&quot;Taras&quot;: &quot;<?php the_field( 'flat_surface_terrace' ); ?> m<sup>2</sup>&quot;<?php endif;?> ,&quot;Balkon&quot;: &quot;<?php the_field( 'flat_surface_balcony' ); ?> m<sup>2</sup>&quot;}" coords="<?php echo $area; ?>">
                                             <? endif; endif; wp_reset_postdata(); endforeach?>                                             
                                             
                                          </map>
                                       </div>

                                          <ul class="flat-list">
                                             <?php 
                                             $posts = get_posts(array(
                                                'post_type' => 'flat',
                                                'posts_per_page' => 100
                                             ));
                                             foreach($posts as $post): 
                                             setup_postdata($post);
                                             $area = get_field('flat_area');
                                             $flat_floor = get_field('flat_floor');
                                             $postid = get_the_ID();
                                             $flat_status = get_field('flat_status');
                                             $flat_pdf = get_field( 'flat_pdf' );
                                             ?>
                                             <?php if ($flat_status == "wolne" and $flat_floor == '3'): ?>
                                             <?php
                                             get_template_part( 'template-parts/partial', 'flat-cnt');
                                             ?>
                                          <? endif; wp_reset_postdata(); endforeach?>    
                                       </ul>
      </div>
   </li>

</ul>
