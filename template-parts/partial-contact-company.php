<?php
/**
 * ===============================
 * CONTACT COMAPANY.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="contact-company">
    <div class="container">
        <div class="row">
            <div class="title-small" data-aos="fade-up">  
            <?php echo _e('dane kontaktowe','rg');?>              
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="name" data-aos="fade-up">
                    <?php the_field( 'contact_company_name' ); ?>
                </div>
                <div class="info" data-aos="fade-up">
                    <?php the_field( 'contact_company_adress' ); ?>
                </div>
                <div class="info" data-aos="fade-up">
                    <?php the_field( 'contact_company_nip' ); ?>
                </div>
            </div>
        </div>
    </div>
</div>