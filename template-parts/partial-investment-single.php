<?php
/**
 * ===============================
 * INVESTMENT SINGLE.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<?php if ( have_rows( 'invest_single_cnt' ) ): ?>
<div class="investment-single">

    <?php while ( have_rows( 'invest_single_cnt' ) ) : the_row(); ?>
    
    <?php if ( get_row_layout() == 'invest_single_lead' ) : ?>
    <!-- LEAD -->
    <div class="investment-single-lead" data-aos="fade-up" id="<?php the_sub_field( 'invest_single_lead_id' ); ?>">
        <?php the_sub_field( 'invest_single_lead_cnt' ); ?>
    </div>

    <?php elseif ( get_row_layout() == 'invest_single_block_two_photo' ) : ?>
    <div class="investment-single-two-photo">
        <div class="foto1">
            <?php $size = 'full'; ?>
            <?php $invest_single_block_two_photo_img_1 = get_sub_field( 'invest_single_block_two_photo_img_1' ); ?>
            <?php if ( $invest_single_block_two_photo_img_1 ) : ?>
                <?php echo wp_get_attachment_image( $invest_single_block_two_photo_img_1, $size, false, [
                    'class' => 'lazyload img-fluid',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $invest_single_block_two_photo_img_1 , $size ),
                    'alt' => get_post_meta( $invest_single_block_two_photo_img_1 , '_wp_attachment_image_alt', true),
                    ]); 
                ?>
            <?php endif; ?>
        </div>
        <div class="cnt" data-aos="fade-up">

            <?php $invest_single_block_two_photo_title = get_sub_field( 'invest_single_block_two_photo_title');
            $invest_single_block_two_photo_cnt = get_sub_field( 'invest_single_block_two_photo_cnt' )
            ?>
            <?php if ($invest_single_block_two_photo_title): ?>
                <div class="title-small">
                    <?php echo $invest_single_block_two_photo_title; ?>
                </div>
                <?php echo $invest_single_block_two_photo_cnt; ?>
            <?php endif ?>
        </div>
        <div class="foto2">
            <?php $invest_single_block_two_photo_img_2 = get_sub_field( 'invest_single_block_two_photo_img_2' ); ?>
            <?php $size = 'full'; ?>
            <?php if ( $invest_single_block_two_photo_img_2 ) : ?>
                <?php echo wp_get_attachment_image( $invest_single_block_two_photo_img_2, $size, false, [
                    'class' => 'lazyload img-fluid',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $invest_single_block_two_photo_img_2 , $size ),
                    'alt' => get_post_meta( $invest_single_block_two_photo_img_2 , '_wp_attachment_image_alt', true),
                    ]); 
                ?>
            <?php endif; ?>
        </div>
    </div>


    <?php elseif ( get_row_layout() == 'invest_single_advantages' ) : ?>
    <?php $invest_single_advantages_title = get_sub_field( 'invest_single_advantages_title' ); ?>
    <?php $invest_single_advantages_id = get_sub_field( 'invest_single_advantages_id' ); ?>
    <!-- ATUTY -->
    <div class="investment-single-advantages" <?php if ( $invest_single_advantages_id): ?>id="<?php echo  $invest_single_advantages_id ?>"<?php endif ?>>
        <div class="container">
            <?php if ($invest_single_advantages_title): ?>
            <div class="title-big" data-aos="fade-up">
                <?php echo $invest_single_advantages_title ?>
            </div>
            <?php endif ?>
            <div data-aos="fade-up">
                <?php the_sub_field( 'invest_single_advantages_cnt' ); ?>                
            </div>

            <?php if ( have_rows( 'invest_single_advantages_box' ) ) : ?>
                <ul>
                    <?php while ( have_rows( 'invest_single_advantages_box' ) ) : the_row(); ?>
                    <li>
                        <?php $invest_single_advantages_box_img = get_sub_field( 'invest_single_advantages_box_img' ); ?>
                        <?php $size = 'full'; ?>
                        <?php if ( $invest_single_advantages_box_img ) : ?>
                            <div class="image" data-aos="fade-up">
                                <?php echo wp_get_attachment_image( $invest_single_advantages_box_img, $size ); ?>
                            </div>
                        <?php endif; ?>
                        <?php $invest_single_advantages_box_cnt = get_sub_field( 'invest_single_advantages_box_cnt' );?>
                        <?php if ($invest_single_advantages_box_cnt): ?>
                            <p data-aos="fade-up"><?php echo $invest_single_advantages_box_cnt ?></p>
                        <?php endif ?>
                    </li>
                <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>

    <?php elseif ( get_row_layout() == 'invest_single_blue_box' ) : ?>
    <?php $invest_single_blue_box_id = get_sub_field('invest_single_blue_box_id'); ?>
    <!-- BLUE BOX -->
    <div class="investment-single-blue-box" <?php if ($invest_single_blue_box_id): ?>id="<?php echo $invest_single_blue_box_id ?>"<?php endif ?>>
        <div class="container">
            <?php $invest_single_blue_box_title = get_sub_field( 'invest_single_blue_box_title' );
            $invest_single_blue_box_cnt = get_sub_field( 'invest_single_blue_box_cnt' ); ?>
            <?php if ($invest_single_blue_box_title): ?>
                <div class="title-middle" data-aos="fade-up">
                    <?php echo $invest_single_blue_box_title ?>
                </div>
            <?php endif ?>
            <?php if ($invest_single_blue_box_cnt): ?>
                <div data-aos="fade-up"><?php echo $invest_single_blue_box_cnt; ?></div>
            <?php endif ?>

            <?php $invest_single_blue_box_link = get_sub_field( 'invest_single_blue_box_link' ); ?>
            <?php if ( $invest_single_blue_box_link ) : ?>
                <div data-aos="fade-up" class="link">
                    <a href="<?php echo esc_url( $invest_single_blue_box_link['url'] ); ?>" class="btn-white"><?php echo esc_html( $invest_single_blue_box_link['title'] ); ?></a>
                </div>
            <?php endif; ?>

            <div class="image" data-aos="fade-up">
                <?php $invest_single_blue_box_img = get_sub_field( 'invest_single_blue_box_img' ); ?>
                <?php $size = 'full'; ?>
                <?php if ( $invest_single_blue_box_img ) : ?>
                    <?php echo wp_get_attachment_image( $invest_single_blue_box_img, $size, false, [
                        'class' => 'lazyload img-fluid',
                        'loading' => 'lazy',
                        'data-src' => wp_get_attachment_image_url( $invest_single_blue_box_img , $size ),
                        'alt' => get_post_meta( $invest_single_blue_box_img , '_wp_attachment_image_alt', true),
                        ]); 
                    ?>
                <?php endif; ?> 
            </div>            
        </div>
    </div>


    <?php elseif ( get_row_layout() == 'invest_single_box_50_50' ) : ?>
    <!-- BOX 50 -->
    <div class="investment-single-box-50-50" id="<?php the_sub_field( 'invest_single_box_50_50_id' ); ?>">
        <?php $invest_single_box_50_50_title = get_sub_field( 'invest_single_box_50_50_title' ); ?>
        <?php if ($invest_single_box_50_50_title): ?>
            <div class="container">
                <div class="title-big" data-aos="fade-up">
                    <?php echo $invest_single_box_50_50_title; ?>
                </div>
            </div>
        <?php endif ?>
        <?php if ( have_rows( 'invest_single_box_50_50_box' ) ) : ?>
            <ul>
                <?php while ( have_rows( 'invest_single_box_50_50_box' ) ) : the_row(); ?>
                    <li class="row">
                        <div class="col cnt full">
                            <div class="txt" data-aos="fade-up">
                                <?php $invest_single_box_50_50_box_title = get_sub_field( 'invest_single_box_50_50_box_title' ); ?>
                                <?php if ($invest_single_box_50_50_box_title): ?>
                                    <div class="title-middle">
                                        <?php echo $invest_single_box_50_50_box_title;?>
                                    </div>
                                <?php endif ?>
                                <?php the_sub_field( 'invest_single_box_50_50_box_cnt' ); ?>

                                <?php $invest_single_box_50_50_box_link_cnt = get_sub_field('invest_single_box_50_50_box_link_cnt') ?>
                                <?php if ($invest_single_box_50_50_box_link_cnt): ?>
                                    <a href="<?php the_sub_field( 'invest_single_box_50_50_box_link' ); ?>" class="btn-blue"><?php echo $invest_single_box_50_50_box_link_cnt ?></a>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="col image">
                            <div class="swiper gallBox swiper-backface-hidden">
                                <?php if ( have_rows( 'invest_single_box_50_50_box_slider' ) ) : ?>
                                <div class="swiper-wrapper">
                                <?php while ( have_rows( 'invest_single_box_50_50_box_slider' ) ) : the_row(); ?>
                                    <div class="swiper-slide">
                                    <?php $invest_single_box_50_50_box_slider_img = get_sub_field( 'invest_single_box_50_50_box_slider_img' ); ?>
                                    <?php $size = 'image960'; ?>
                                    <?php $thumb = $invest_single_box_50_50_box_slider_img['sizes'][ $size ]; ?>
                                    
                                    <?php if ( $invest_single_box_50_50_box_slider_img ) : ?>
                                        <img width="960" height="723" data-src="<?php echo esc_url($thumb); ?>" class="swiper-lazy" />
                                        <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                                    <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                                </div>
                                <?php endif; ?>
                                <div class="swiper-pagination"></div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>                            
                        </div>

                        <?php $invest_single_box_50_50_box_cnt_left = get_sub_field( 'invest_single_box_50_50_box_cnt_left' );
                        if ($invest_single_box_50_50_box_cnt_left): ?>
                        <div class="row-info">
                            <h3 class="full" data-aos="fade-up">
                                <?php echo _e('Elementy wyposażenia','rg');?>
                            </h3>
                            <div class="row-info-container full" data-aos="fade-up">
                                <div class="row-info-col">
                                   <?php the_sub_field( 'invest_single_box_50_50_box_cnt_left' ); ?>
                                </div>
                                <div class="row-info-col">
                                   <?php the_sub_field( 'invest_single_box_50_50_box_cnt_right' ); ?>
                                </div>    
                            </div>
                                <div class="row-info-small container" data-aos="fade-up">
                                    <?php the_sub_field( 'invest_single_box_50_50_box_cnt_info' ); ?>
                                </div>
                            
                        </div>    
                        <?php endif ?>

                    </li>                
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>


    <?php elseif ( get_row_layout() == 'invest_single_contractors' ) : ?>
        <?php $invest_single_contractors_id = get_sub_field( 'invest_single_contractors_id' ); ?>
    <!-- WYKONAWCY -->
    <div class="investment-single-contractors full" <?php if ($invest_single_contractors_id): ?>id="<?php echo $invest_single_contractors_id ?>"<?php endif ?>>
        <?php $invest_single_contractors_bg = get_sub_field( 'invest_single_contractors_bg' ); ?>
        <?php $size = 'full'; ?>
        <?php if ( $invest_single_contractors_bg ) : ?>
            <?php echo wp_get_attachment_image( $invest_single_contractors_bg, $size ); ?>
        <?php endif; ?>

        <?php $invest_single_contractors_subtitle = get_sub_field( 'invest_single_contractors_subtitle' );
        $invest_single_contractors_title = get_sub_field( 'invest_single_contractors_title' );
        $invest_single_contractors_cnt = get_sub_field( 'invest_single_contractors_cnt' ); 
        ?>
        <div class="txt">
            <?php if ($invest_single_contractors_subtitle): ?>
            <div class="subtitle" data-aos="fade-up">
                <?php echo $invest_single_contractors_subtitle; ?>
            </div>
            <?php endif ?>
            <?php if ($invest_single_contractors_title): ?>
                <div class="title-big" data-aos="fade-up">
                    <?php echo $invest_single_contractors_title; ?>
                </div>
            <?php endif ?>
            <?php if ($invest_single_contractors_cnt): ?>
                <div data-aos="fade-up">
                    <?php echo $invest_single_contractors_cnt; ?>
                </div>
            <?php endif ?>
        </div>
    </div>

    <?php elseif ( get_row_layout() == 'invest_single_gallery' ) : ?>
    <?php $invest_single_gallery_title = get_sub_field( 'invest_single_gallery_title' );  ?>

    <!-- GALLERY -->
    <div class="investment-single-gallery" id="<?php the_sub_field( 'invest_single_gallery_id' ); ?>">
        <div class="container">
            <?php if ($invest_single_gallery_title): ?>
                <div class="title-big" data-aos="fade-up">
                    <?php echo $invest_single_gallery_title; ?>
                </div>
            <?php endif ?>
            <?php $invest_single_gallery_img_images = get_sub_field( 'invest_single_gallery_img' ); ?>
            <?php if ( $invest_single_gallery_img_images ) :  ?>
            <ul>
                <?php foreach ( $invest_single_gallery_img_images as $invest_single_gallery_img_image ): ?>
                    <li>
                        <a href="<?php echo esc_url( $invest_single_gallery_img_image['url'] ); ?>" data-fancybox="gallery" data-aos="fade-up">
                            <img src="<?php echo esc_url( $invest_single_gallery_img_image['sizes']['image400'] ); ?>" alt="<?php echo esc_attr( $invest_single_gallery_img_image['alt'] ); ?>" />
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
        </div>
    </div>


    <?php elseif ( get_row_layout() == 'invest_single_slider' ) : ?>
    <!-- GALLERY -->
    <div class="investment-single-slider" id="<?php the_sub_field( 'invest_single_slider_id' ); ?>">
        <ul class="swiper-wrapper">
            <?php if ( have_rows( 'invest_single_slider_loop' ) ) : ?>
                <?php while ( have_rows( 'invest_single_slider_loop' ) ) : the_row(); ?>
                    <li class="swiper-slide">
                        <?php $invest_single_slider_loop_img = get_sub_field( 'invest_single_slider_loop_img' ); ?>
                        <?php $size = 'full'; ?>
                        <?php if ( $invest_single_slider_loop_img ) : ?>
                            <?php echo wp_get_attachment_image( $invest_single_slider_loop_img, $size, false, [
                                'class' => 'lazyload img-fluid',
                                'loading' => 'lazy',
                                'data-src' => wp_get_attachment_image_url( $invest_single_slider_loop_img , $size ),
                                'alt' => get_post_meta( $invest_single_slider_loop_img , '_wp_attachment_image_alt', true),
                                ]); 
                            ?>
                        <?php endif; ?>

                        <?php $invest_single_slider_loop_title = get_sub_field( 'invest_single_slider_loop_title' );
                        $invest_single_slider_loop_subtitle = get_sub_field( 'invest_single_slider_loop_subtitle' ) ?>
                        <div class="txt full">
                            <?php if ($invest_single_slider_loop_title): ?>
                                <div class="title-middle" data-aos="fade-up">
                            <?php the_sub_field( 'invest_single_slider_loop_title' ); ?>    
                                </div>    
                            <?php endif ?>
                            <?php if ($invest_single_slider_loop_subtitle): ?>
                                <p data-aos="fade-up"><?php the_sub_field( 'invest_single_slider_loop_subtitle' ); ?></p>
                            <?php endif ?>
                        </div>
                    </li>
                <?php endwhile; ?>
            <?php endif; ?>
        </ul>
    </div>


    <?php elseif ( get_row_layout() == 'invest_single_search' ) : ?>
    <!-- SEARCH -->
    <div class="investment-single-search">
        <div class="container">            
            <?php the_sub_field( 'invest_single_search_lead' ); ?>

            <?php if ( have_rows( 'invest_single_search_icons' ) ) : ?>
                <ul class="list">
                    <?php while ( have_rows( 'invest_single_search_icons' ) ) : the_row(); ?>
                    <li>
                        <?php $invest_single_search_icons_img = get_sub_field( 'invest_single_search_icons_img' ); ?>
                        <?php $size = 'full'; ?>
                        <?php if ( $invest_single_search_icons_img  ) : ?>
                            <div class="image" data-aos="fade-up">
                                <?php echo wp_get_attachment_image( $invest_single_search_icons_img , $size ); ?>
                            </div>
                        <?php endif; ?>
                        <?php $invest_single_search_icons_txt = get_sub_field( 'invest_single_search_icons_txt' );?>
                        <?php if ($invest_single_search_icons_txt): ?>
                            <p data-aos="fade-up"><?php echo $invest_single_search_icons_txt ?></p>
                        <?php endif ?>
                    </li>
                <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>

        <div id="<?php the_sub_field( 'invest_single_search_id' ); ?>">
            <div class="container">
                <div class="title-big"><?php the_sub_field( 'invest_single_search_title' ); ?></div>
                <?php get_template_part( 'template-parts/partial', 'flat-search-3d'); ?>
                <?php get_template_part( 'template-parts/partial', 'flat-search'); ?>
            </div>
        </div>
    </div>

    <?php endif; ?> 

    <?php endwhile; ?>   

</div>
<?php endif;?>

<div class="investment-form" id="kontakt">
    <div class="container">
        <?php $contact_title_widget = get_field( 'contact_title_widget' ); ?>
        <h3 class="title-big" data-aos="fade-up">
            <?php if ($contact_title_widget): ?>
                <?php echo $contact_title_widget ?>
            <?php else: ?>
                <?php echo _e('Napisz do nas', 'rg');?>
            <?php endif ?>
        </h3>

        <div data-aos="fade-up">
            <?php echo do_shortcode('[contact-form-7 id="186" title="Inwestycja"]'); ?>
        </div>
    </div>
</div>

<?php if ( have_rows( 'invest_single_cnt' ) ): ?>
<?php while ( have_rows( 'invest_single_cnt' ) ) : the_row(); ?>
    
<?php if ( get_row_layout() == 'invest_single_maps' ) : ?> 
    <?php $invest_single_maps_iframe = get_sub_field( 'invest_single_maps_iframe' ); ?>
<div class="acf-map">

    <div class="marker" data-lat="<?php echo $invest_single_maps_iframe['lat']; ?>" data-lng="<?php echo $invest_single_maps_iframe['lng']; ?>" data-icon="/wp-content/uploads/2022/05/marina-ilawa.png">
      </div>
</div>
<?php endif; ?> 
<?php endwhile; ?>   
<?php endif;?>