<?php
/**
 * ===============================
 * PARTIAL BLUE BOX.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
$blue_box_title = get_field( 'blue-box-title' );
$blue_box_cnt = get_field( 'blue-box-cnt' )
?>
<div class="blue-box">

    <div class="blue-box-header">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/svg/img-bg-blue-box.svg" alt="">
    </div>

    <div class="container">
        <?php if ($blue_box_title): ?>
            <h3 class="typo1" data-aos="fade-up">
                <?php echo $blue_box_title; ?>
            </h3>
        <?php endif ?>
        <?php if ($blue_box_cnt): ?>
            <div data-aos="fade-up">
                <?php the_field( 'blue-box-cnt' ); ?> 
            </div>   
        <?php endif ?>


        <?php if ( have_rows( 'blue-box-boxes' ) ) : ?>
            <ul class="list">
                <?php while ( have_rows( 'blue-box-boxes' ) ) : the_row(); ?>
                    <li>
                        <?php $blue_box_boxes_icon = get_sub_field( 'blue-box-boxes-icon' ); ?>
                        <?php $size = 'full'; ?>
                        <?php if ( $blue_box_boxes_icon ) : ?>
                            <div class="icon" data-aos="fade-up">
                                <?php echo wp_get_attachment_image( $blue_box_boxes_icon, $size, false, [
                                    'class' => 'lazyload',
                                    'loading' => 'lazy',
                                    'data-src' => wp_get_attachment_image_url( $blue_box_boxes_icon , $size ),
                                    'alt' => get_post_meta( $blue_box_boxes_icon , '_wp_attachment_image_alt', true),
                                    ]); 
                                ?>
                            </div>
                        <?php endif; ?>
                        <p data-aos="fade-up"><?php the_sub_field( 'blue-box-boxes-cnt' ); ?></p>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif;?>
        
    </div>

</div>