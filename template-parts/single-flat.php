<?php 
$types = get_terms( array(
    'taxonomy' => 'cat-type',
    'hide_empty' => false,
) );
?>
<ul>
    <li>
        <span>Nr aparatmentu:</span> <?php echo get_the_title() ; ?>
    </li>
    <li>
        <span>Piętro:</span> <?php echo get_field('flat_floor') == 0 ? 'parter' : get_field('flat_floor'); ?>
    </li>
    <li>
        <span>Ilość pokoi:</span> <?php echo get_field('flat_room')?>
    </li>
    <li>
        <span>Powierzchnia:</span> <?php echo get_field('flat_surface')?> m<sup>2</sup></li>
    <li>
        <span>Typ:</span> 
        <?php $terms = get_the_terms($post->ID, 'cat-type' );?>
        <?php if ($terms): ?>
            <?php foreach ($terms as $term) {echo "".$term_name = $term->name.'';}?>
        <?php endif ?>
    </li>
    <li>
        <?php if (get_field('flat_status') != 'sprzedane'): ?>
        <a href="#" class="price" data-offer-nr="<?php echo get_the_title() ; ?>" data-offer-surface="<?php echo get_field('flat_surface')?>" data-offer-floor="<?php echo get_field('flat_floor') == 0 ? 'Parter' : get_field('flat_floor'); ?>" data-offer-room="<?php echo get_field('flat_room')?>">
            <?php _e('Zapytaj o cenę', 'rg'); ?></a>
        <?php endif;?>
    </li>
    <li>
        <span>Status:</span> <?php echo get_field('flat_status'); ?>
    </li>
    <li>
        <?php if (get_field('flat_status') != 'sprzedane'): ?>
            
            <?php
                $inwestycja  = get_the_terms($post->ID, 'cat-flat' );
                $folder      = get_field('folder', 'term_'.$inwestycja[0]->term_id);
                $nazwa_pliku = get_field('nazwa_pliku', 'term_'.$inwestycja[0]->term_id);
                $pdf         = $folder . '/' . $nazwa_pliku . get_the_title() . '.pdf'; 
            ?>

            <?php if(file_exists( get_template_directory() .'/assets/plany/'.$pdf )) : ?>
                <a target="_blank" class="pdf" href="<?php echo get_template_directory_uri() .'/assets/plany/' . $pdf; ?>">
                    <?php _e('Zobacz plan', 'rg'); ?>
                </a>
            <?php endif; ?>

        <?php endif ?>
    </li>
</ul>