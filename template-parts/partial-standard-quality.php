<?php
/**
 * ===============================
 * STADNARD QUALITY.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="banner">
    <?php $standard_quality_img = get_field( 'standard_quality_img' ); ?>
    <?php $size = 'full'; ?>
    <?php echo wp_get_attachment_image( $standard_quality_img, $size, false, [
        'class' => 'lazyload img-fluid',
        'loading' => 'lazy',
        'data-src' => wp_get_attachment_image_url( $standard_quality_img , $size ),
        'alt' => get_post_meta( $standard_quality_img , '_wp_attachment_image_alt', true),
        ]); 
    ?>
    <?php $standard_quality_subtitle = get_field( 'standard_quality_subtitle' );
        $standard_quality_title = get_field( 'standard_quality_title' );
        $standard_quality_cnt = get_field( 'standard_quality_cnt' ); 
        ?>
    <div class="txt full">
        <?php if ($standard_quality_subtitle): ?>
        <div class="subtitle" data-aos="fade-up">
            <?php echo $standard_quality_subtitle; ?>
        </div>
        <?php endif ?>
        <?php if ($standard_quality_title): ?>
        <div class="title-big" data-aos="fade-up">
            <?php echo $standard_quality_title; ?>
        </div>
        <?php endif ?>
        <?php if ($standard_quality_cnt): ?>
        <div data-aos="fade-up">
            <?php echo $standard_quality_cnt; ?>
        </div>
        <?php endif ?>
    </div>
</div>