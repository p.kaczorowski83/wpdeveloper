<?php
/**
 * ===============================
 * ABOUT OFFICE .PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<div class="about-office">
    <div class="row">
        <div class="col full">
            <div class="about-office-txt">
                <?php $about_office_subtitle = get_field('about_office_subtitle');
                $about_office_title = get_field('about_office_title');
                $about_office_cnt = get_field('about_office_cnt'); ?>
                <?php if ($about_office_subtitle): ?>
                    <h3 data-aos="fade-up">
                        <?php echo $about_office_subtitle ?>
                    </h3>
                <?php endif ?>
                <?php if ($about_office_title): ?>
                    <h2 data-aos="fade-up">
                        <?php echo $about_office_title ?>
                    </h2>
                <?php endif ?>
                <?php if ($about_office_cnt): ?>
                    <div data-aos="fade-up">
                        <?php echo $about_office_cnt ?>
                    </div>
                <?php endif ?>

            </div>
        </div>
        <div class="col">
            <?php $about_office_img = get_field( 'about_office_img' ); ?>
            <?php $size = 'full'; ?>
            <?php if ( $about_office_img ) : ?>
                <?php echo wp_get_attachment_image( $about_office_img, $size, false, [
                    'class' => 'lazyload img-fluid',
                    'loading' => 'lazy',
                    'data-src' => wp_get_attachment_image_url( $about_office_img , $size ),
                    'alt' => get_post_meta( $about_office_img , '_wp_attachment_image_alt', true),
                    'data-aos' => "fade-up"
                    ]); 
                ?>
            <?php endif; ?>
        </div>
    </div>
</div>