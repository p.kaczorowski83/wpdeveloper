<?php
/**
 * ===============================
 * PARTIAL BO 50/50.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<?php if ( have_rows( 'boxes' ) ) : ?>
<div class="box-50-50">
    <ul>
        <?php while ( have_rows( 'boxes' ) ) : the_row();
            $boxes_title = get_sub_field( 'boxes_title' );
            $boxes_cnt = get_sub_field( 'boxes_cnt' ); ?>
            <li>
                <div class="waves" data-parallax="{&quot;y&quot;: -50}"></div>
                <div class="text">
                    <?php if ( $boxes_title ): ?>
                        <h4>
                           <?php echo $boxes_title; ?>
                        </h4>
                    <?php endif; ?>
                    <?php echo $boxes_cnt; ?>
                    <?php $boxes_link = get_sub_field( 'boxes_link' ); ?>
                    <?php if ( $boxes_link ) : ?>
                        <a href="<?php echo esc_url( $boxes_link['url'] ); ?>" class="btn-base" target="_blank"><?php echo esc_html( $boxes_link['title'] ); ?></a>
                    <?php endif; ?>
                </div>
                <div class="foto">
                    <?php $boxes_img = get_sub_field( 'boxes_img' ); ?>
                    <?php $size = 'image1230'; ?>
                    <?php if ( $boxes_img ) : ?>
                        <?php echo wp_get_attachment_image( $boxes_img , $size, false, [
                            'class' => 'lazyload img-fluid',
                            'loading' => 'lazy',
                            'data-src' => wp_get_attachment_image_url( $boxes_img , $size ),
                            'alt' => get_post_meta( $boxes_img , '_wp_attachment_image_alt', true),
                            'data-aos' => "fade-up"
                            ]); 
                        ?>
                    <?php endif; ?>
                </div>
            </li>
        <?php endwhile; ?>
    </ul>
</div>
<?php endif; ?>