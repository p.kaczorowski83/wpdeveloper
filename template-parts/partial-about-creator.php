<?php
/**
 * ===============================
 * ABOUT CREATOR .PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
$about_creator_title = get_field( 'about_creator_title' );
?>
<?php if ($about_creator_title): ?>
<div class="about-creator-header">
    <div class="container">
        <h2 data-aos="fade-up">
            <?php echo $about_creator_title ?>
        </h2>
    </div>
</div>    

<!-- SLIDER -->
<div class="about-creator-slider">
    <?php if ( have_rows( 'about_creator_slider' ) ) : ?>
        <div class="swiper-wrapper">
            <?php while ( have_rows( 'about_creator_slider' ) ) : the_row(); ?>
                <div class="swiper-slide">
                    <div class="foto">
                        <?php $about_creator_slider_img = get_sub_field( 'about_creator_slider_img' ); ?>
                        <?php $size = 'imageAbout'; ?>
                        <?php if ( $about_creator_slider_img ) : ?>
                            <?php echo wp_get_attachment_image( $about_creator_slider_img, $size, false, [
                                'class' => 'lazyload img-fluid',
                                'loading' => 'lazy',
                                'data-src' => wp_get_attachment_image_url( $about_creator_slider_img , $size ),
                                'alt' => get_post_meta( $about_creator_slider_img , '_wp_attachment_image_alt', true),
                                ]); 
                            ?>
                        <?php endif; ?>
                    </div>
                    <div class="cnt">
                        <div class="row">
                            <div class="block">
                                <div class="title"><?php the_sub_field( 'about_creator_slider_title' ); ?></div>
                                <div><?php the_sub_field( 'about_creator_slider_cnt' ); ?></div>
                                <?php $about_creator_slider_link = get_sub_field( 'about_creator_slider_link' );
                                if ($about_creator_slider_link) :?>
                                    <a href="<?php echo $about_creator_slider_link ?>" class="btn-white" target="_blank"><?php echo _e('Strona internetowa', 'rg') ?></a>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    <?php endif; ?>
</div>
<?php endif ?>
