<?php
/**
 * ===============================
 * INVESTMENT ON SALE.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>
<?php $investment_preparation_list = get_field( 'investment_preparation_list' ); ?>
<?php $investment_sold_list = get_field( 'investment_sold_list' ); ?>
<?php if ( $investment_sold_list ) : ?>
<section class="investment-list">
    <div class="container">
        <h3 data-aos="fade-up"><span><?php if ( $investment_preparation_list ) : ?>03 <?php else: ?>02<?php endif;?></span><?php the_field( 'investment_sold_title' ); ?></h3> 

        <ul>
        <?php foreach ( $investment_sold_list as $post ) : ?>
            <?php setup_postdata ( $post ); ?>
            <li data-aos="fade-up">
                <a href="<?php the_permalink(); ?>">
                    <?php $invest_single_img = get_field( 'invest_single_img' ); ?>
                    <?php $size = 'full'; ?>
                    <?php if ( $invest_single_img ) : ?>
                        <div class="image">
                            <?php echo wp_get_attachment_image( $invest_single_img, $size, false, [
                                'class' => 'lazyload img-fluid',
                                'loading' => 'lazy',
                                'data-src' => wp_get_attachment_image_url( $invest_single_img , $size ),
                                'alt' => get_post_meta( $invest_single_img , '_wp_attachment_image_alt', true),
                                ]); 
                            ?>
                        </div>
                    <?php endif; ?>
                    <p><?php the_title(); ?></p>
                </a>
            </li>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
        </ul>

    </div><!-- end .container -->
</section>
<?php endif; ?>