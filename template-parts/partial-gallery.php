<?php
/**
 * ===============================
 * POPUP.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<div class="gallery" style="display: none;">
    <?php $gallery_footer_images = get_field( 'gallery_footer', 'option' ); ?>
    <?php if ( $gallery_footer_images ) :  ?>
        <?php foreach ( $gallery_footer_images as $gallery_footer_image ): ?>
            <a href="<?php echo esc_url( $gallery_footer_image['url'] ); ?>" data-fancybox="gallery-apla" data-caption="<?php echo esc_html( $gallery_footer_image['title'] ); ?>">
                <img src="<?php echo esc_url( $gallery_footer_image['sizes']['thumbnail'] ); ?>" alt="<?php echo esc_attr( $gallery_footer_image['alt'] ); ?>" />
            </a>
        <?php endforeach; ?>
    <?php endif; ?>
</div>