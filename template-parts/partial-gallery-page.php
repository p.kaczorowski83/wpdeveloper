<?php
/**
 * ===============================
 * POPUP.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<div class="gallery">
    <div class="container">
        <?php $gallery_footer_images = get_field( 'gallery_footer', 'option' ); ?>
        <ul>
        <?php foreach ( $gallery_footer_images as $gallery_footer_image ): ?>
            <li>
                <a href="<?php echo esc_url( $gallery_footer_image['url'] ); ?>" data-fancybox="gallery-apla" data-caption="<?php echo esc_html( $gallery_footer_image['title'] ); ?>">
                    <img src="<?php echo esc_url( $gallery_footer_image['sizes']['imageNews'] ); ?>" alt="<?php echo esc_attr( $gallery_footer_image['alt'] ); ?>" />
                    <p><?php echo esc_html( $gallery_footer_image['title'] ); ?></p>
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>