<?php
/**
 * ===============================
 * PARTIAL DEVELOPER REALIZATIONS.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
$developer_realizations_title = get_field( 'developer_realizations_title' );
?>

<div class="developer-realizations">

    <div class="container">
        <?php if ( $developer_realizations_title ): ?>
            <h3 class="typo1" data-aos="fade-up">
                <?php echo $developer_realizations_title; ?>
            </h3>
        <?php endif ?>

        <?php if ( have_rows( 'developer_realizations' ) ) : ?>
            <ul>
                <?php while ( have_rows( 'developer_realizations' ) ) : the_row(); ?>
                    <li>
                        <div class="box" data-aos="fade-up">
                        <?php $developer_realizations_link = get_sub_field( 'developer_realizations_link' ); 
                        $developer_realizations_logo = get_sub_field( 'developer_realizations_logo' ); ?>
                        <?php $size = 'full'; ?>
                        <?php if ( $developer_realizations_link ): ?>
                            <a href="<?php echo $developer_realizations_link;?>" target="_blank">
                                <?php if ( $developer_realizations_logo ) : ?>
                                    <?php echo wp_get_attachment_image( $developer_realizations_logo, $size, false, [
                                        'class' => 'lazyload',
                                        'loading' => 'lazy',
                                        'data-src' => wp_get_attachment_image_url( $developer_realizations_logo , $size ),
                                        'alt' => get_post_meta( $developer_realizations_logo , '_wp_attachment_image_alt', true),
                                        ]); 
                                    ?>
                                <?php endif; ?>
                            </a>
                        <?php else: ?>
                            <?php if ( $developer_realizations_logo ) : ?>
                                <?php echo wp_get_attachment_image( $developer_realizations_logo, $size ); ?>
                            <?php endif; ?>
                        <?php endif ?>                        
                        </div>
                    </li>   
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>

</div>