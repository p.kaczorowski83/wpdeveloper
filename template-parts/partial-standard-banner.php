<?php
/**
 * ===============================
 * STADNARD BANNER.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="banner">
    <?php $standard_contractors_img = get_field( 'standard_contractors_img' ); ?>
    <?php $size = 'full'; ?>
    <?php echo wp_get_attachment_image( $standard_contractors_img, $size, false, [
        'class' => 'lazyload img-fluid',
        'loading' => 'lazy',
        'data-src' => wp_get_attachment_image_url( $standard_contractors_img , $size ),
        'alt' => get_post_meta( $standard_contractors_img , '_wp_attachment_image_alt', true),
        ]); 
    ?>
    <?php $standard_contractors_subtitle = get_field( 'standard_contractors_subtitle' );
        $standard_contractors_title = get_field( 'standard_contractors_title' );
        $standard_contractors_cnt = get_field( 'standard_contractors_cnt' ); 
        ?>
    <div class="txt full">
        <?php if ($standard_contractors_subtitle): ?>
        <div class="subtitle" data-aos="fade-up">
            <?php echo $standard_contractors_subtitle; ?>
        </div>
        <?php endif ?>
        <?php if ($standard_contractors_title): ?>
        <div class="title-big" data-aos="fade-up">
            <?php echo $standard_contractors_title; ?>
        </div>
        <?php endif ?>
        <?php if ($standard_contractors_cnt): ?>
        <div data-aos="fade-up">
            <?php echo $standard_contractors_cnt; ?>
        </div>
        <?php endif ?>
    </div>
</div>