<?php
/**
 * ===============================
 * HEADER.PHP - main header file
 * ===============================
 *
 * @package BEGO
 * @since 1.0.0
 * @version 1.0.0
 */
$site_title = get_bloginfo( 'name' );
$site_description = get_bloginfo( 'description' );
?>
<?php 
get_template_part( 'template-parts/partial', 'mobile-menu' ); 
?>	

<?php if ( !is_404() ): ?>
<header class="navbar__fixed">	
	<div class="navbar__fixed-cnt">

		<!-- LOGO -->
		<figure class="navbar__fixed-logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/logo.svg" alt="<?php bloginfo('name'); ?>" class="img-fluid">
			</a>		
		</figure>

		<!-- SUBMENU ADN MENU -->
		<?php 
			get_template_part( 'template-parts/partial', 'header-main' ); 
		?>		
	
	</div><!-- end .navbar__fixed-cnt -->	

</header>
<?php endif;?>

<?php 
$invest_single_menu_name = get_field( 'invest_single_menu_name' );
if ( have_rows( 'invest_single_menu' ) ) : ?>
    <div class="submenu">
        <div class="name">
            <?php if ($invest_single_menu_name): ?>
                <?php echo $invest_single_menu_name ?>
            <?php else: ?>
            <?php the_title(); ?>
        <?php endif;?>
        </div>
        <ul>
        <?php while ( have_rows( 'invest_single_menu' ) ) : the_row(); ?>
            <li>
                <a href="<?php the_sub_field( 'invest_single_menu_link' ); ?>">
                    <?php the_sub_field( 'invest_single_menu_name' ); ?>
                </a>
            </li>
        <?php endwhile; ?>
        </ul>        
    </div>
<?php endif;?>


<button class="hamburger">
    <span class="line line1"></span>
    <span class="line line2"></span>
    <span class="line line3"></span>
</button> 