<?php
/**
 * ===============================
 * STANDARD PACK.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<?php if ( have_rows( 'standard_pack' ) ) : ?>
<div class="investment-single-box-50-50">
            <ul>
                <?php while ( have_rows( 'standard_pack' ) ) : the_row(); ?>
                    <li class="row">
                        <div class="col cnt full">
                            <div class="txt" data-aos="fade-up">
                                <?php $title = get_sub_field( 'standard_pack_title' ); ?>
                                <?php if ($title): ?>
                                    <div class="title-middle">
                                        <?php echo $title;?>
                                    </div>
                                <?php endif ?>
                                <p><?php the_sub_field( 'standard_pack_cnt' ); ?></p>
                                <!-- <button class="btn-blue">Dowiedz się więcej</button> -->
                            </div>
                        </div>
                        <div class="col image">
                            <div class="swiper gallBox">
                                <?php if ( have_rows( 'standard_pack_gallery' ) ) : ?>
                                <div class="swiper-wrapper">
                                <?php while ( have_rows( 'standard_pack_gallery' ) ) : the_row(); ?>
                                    <div class="swiper-slide">
                                    <?php $gallery = get_sub_field( 'standard_pack_gallery_img' ); ?>
                                    <?php $size = 'image960'; ?>
                                    <?php $thumb = $gallery['sizes'][ $size ]; ?>
                                    
                                    <?php if ( $gallery ) : ?>
                                        <img width="960" height="723" data-src="<?php echo esc_url($thumb); ?>" class="swiper-lazy" />
                                        <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                                    <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                                </div>
                                <?php endif; ?>
                                <div class="swiper-pagination"></div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>                            
                        </div>

                        <?php $standard_pack_cnt_left = get_sub_field('standard_pack_cnt_left');
                        if ($standard_pack_cnt_left): ?>
                        <div class="row-info">
                            <h3 class="full" data-aos="fade-up">
                                <?php echo _e('Elementy wyposażenia','rg');?>
                            </h3>
                            <div class="row-info-container full">
                                <div class="row-info-col" data-aos="fade-up">
                                    <?php the_sub_field( 'standard_pack_cnt_left' ); ?>
                                </div>
                                <div class="row-info-col" data-aos="fade-up">
                                   <?php the_sub_field( 'standard_pack_cnt_right' ); ?>
                                </div>    
                            </div>
                            <?php $standard_pack_cnt_info = get_sub_field('standard_pack_cnt_info');
                            if ($standard_pack_cnt_info) :?>
                                <div class="row-info-small container" data-aos="fade-up">
                                    <?php echo $standard_pack_cnt_info ?>
                                </div>
                            <?php endif;?>
                            
                        </div>    
                        <?php endif ?>
                    </li>                
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>