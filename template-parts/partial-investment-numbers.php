<?php
/**
 * ===============================
 * PARTIAL INVESTMENT NUMBERS.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
$investment_numbers_title = get_field('investment_numbers_title');
?>

<section class="investment-numbers">

    <div class="waves" data-parallax="{&quot;y&quot;: -50}"></div>

    <div class="container">
        <div class="numbers">
        <ul>
            <?php while ( have_rows( 'investment_numbers' ) ) : the_row(); ?>
                <li data-aos="fade-up">
                    <span class="count"><?php the_sub_field( 'investment_numbers_nr' ); ?></span>
                    <p><?php the_sub_field( 'investment_numbers_text' ); ?></p>
                </li>
            <?php endwhile; ?>        
        </ul>
        </div>
        <div class="text" data-aos="fade-up">
            <?php if ($investment_numbers_title): ?>
                <h3 class="typo3">
                    <?php echo $investment_numbers_title ?>
                </h3>
            <?php endif ?>
            <?php the_field( 'investment_numbers_cnt' ); ?>
        </div>
    </div>
    
</section>