<?php
/**
 * ===============================
 * FOOTER COPY .PHP - footer copy
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="copy">
	
    <div class="container">

        <div class="col">
            <?php if ( have_rows( 'footer_copy', 'option' ) ) : ?>
                <?php while ( have_rows( 'footer_copy', 'option' ) ) : the_row(); ?>
                    <?php $footer_copy_link = get_sub_field( 'footer_copy_link' ); ?>
                    <?php if ( $footer_copy_link ) : ?>
                        <a href="<?php echo esc_url( $footer_copy_link['url'] ); ?>"><?php echo esc_html( $footer_copy_link['title'] ); ?></a>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>

            Copyrights © <?php echo date('Y');?> Realia Group. Wszelkie prawa zastrzeżone
        </div> 

        <div class="col">
            Projekt i realizacja <a href="https://sodova.pl" target="_blank">Sodova</a>
        </div>
        
    </div>   
</div>