<?php
/**
 * ===============================
 * PARTIAL HOME INVESTMENTS.PHP 
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
$home_investments_subtitle = get_field( 'home_investments_subtitle' );
$home_investments_title = get_field('home_investments_title');
?>

<div class="home-investments">
    <?php $home_investments_img = get_field( 'home_investments_img' ); ?>
    <?php $size = 'full'; ?>
    <?php echo wp_get_attachment_image( $home_investments_img, $size, false, [
        'class' => 'lazyload img-fluid',
        'loading' => 'lazy',
        'data-src' => wp_get_attachment_image_url( $home_investments_img , $size ),
        'alt' => get_post_meta( $home_investments_img , '_wp_attachment_image_alt', true),
        ]); 
    ?>

    <div class="txt">
        <div class="container">
            <div class="row">
                <div class="col">
                    <?php if ($home_investments_subtitle): ?>
                    <h3 data-aos="fade-up">
                        <?php echo $home_investments_subtitle ?>
                    </h3>
                    <?php endif ?>
                    <?php if ($home_investments_title): ?>
                        <h2 data-aos="fade-up">
                            <?php echo $home_investments_title ?>
                        </h2>
                    <?php endif ?> 
                    <?php $home_investments_link = get_field( 'home_investments_link' ); ?>
                    <?php if ($home_investments_link): ?>
                        <a href="<?php echo esc_url( $home_investments_link['url'] ); ?>" class="btn-white"><?php echo esc_html( $home_investments_link['title'] ); ?></a>
                    <?php endif ?>   
                </div>
                <div class="col" data-aos="fade-up">
                                     
                </div>
            </div>
        </div>
    </div> 
</div>

<?php $home_investments_list = get_field( 'home_investments_list' ); ?>
<?php if ( $home_investments_list ) : ?>

<div class="home-investments-list">
    <?php $home_investments_slogan = get_field( 'home_investments_slogan' ); ?>
    <?php $size = 'full'; ?>
    <?php if ( $home_investments_slogan ) : ?>
        <div class="slogan" data-aos="fade-up">
            <?php echo wp_get_attachment_image( $home_investments_slogan, $size, false, [
                'class' => 'lazyload img-fluid',
                'loading' => 'lazy',
                'data-src' => wp_get_attachment_image_url( $home_investments_slogan , $size ),
                'alt' => get_post_meta( $home_investments_slogan, '_wp_attachment_image_alt', true),
                ]); 
            ?>
        </div>
    <?php endif; ?>
    <div class="container">
        <ul class="swiper-wrapper">
            <?php foreach ( $home_investments_list as $post ) : ?>
            <?php setup_postdata ( $post ); ?>
                <li class="swiper-slide">
                    <a href="<?php the_permalink();?>" data-aos="fade-up">
                        <div class="image" data-aos="fade-up">
                        <?php $invest_single_img = get_field( 'invest_single_img' ); ?>
                        <?php $size = 'imageInves'; ?>
                        <?php if ( $invest_single_img ) : ?>
                            <?php echo wp_get_attachment_image( $invest_single_img, $size, false, [
                                'class' => 'lazyload img-fluid',
                                'loading' => 'lazy',
                                'data-src' => wp_get_attachment_image_url( $invest_single_img , $size ),
                                'alt' => get_post_meta( $invest_single_img, '_wp_attachment_image_alt', true),
                                ]); 
                            ?>
                        <?php endif; ?>
                    </div>
                    <p data-aos="fade-up"><?php the_title();?></p>
                    </a>
                </li>
            <?php endforeach; ?>            
        </ul>
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</div>
<?php wp_reset_postdata(); ?>
<?php endif; ?>