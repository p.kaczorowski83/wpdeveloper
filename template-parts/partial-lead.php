<?php
/**
 * ===============================
 * LEAD.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
$lead_subtitle = get_field( 'lead_subtitle' );
$lead_title = get_field( 'lead_title' );
$lead_cnt = get_field( 'lead_cnt' );
?>

<section class="lead">
    <div class="container">
        <?php if ($lead_subtitle): ?>
            <h3 data-aos="fade-up">
                <?php echo $lead_subtitle ?>
            </h3>
        <?php endif ?>
        <?php if ($lead_title): ?>
            <h2 data-aos="fade-up">
                <?php echo $lead_title ?>
            </h2>
        <?php endif ?>  
        <?php if ($lead_cnt): ?>
        <div data-aos="fade-up">
            <?php echo $lead_cnt; ?>
        </div>
        <?php endif ?>
    </div>
</section>