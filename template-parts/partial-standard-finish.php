<?php
/**
 * ===============================
 * STANDARD FINISH.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>
   
<div class="investment-single-box-50-50">
     <?php $standard_finish_section = get_field( 'standard_finish_section' );
     $standard_finish_lead = get_field('standard_finish_lead'); ?>
        <?php if ($standard_finish_section): ?>
            <div class="container">
                <div class="title-big" data-aos="fade-up">
                    <?php echo $standard_finish_section; ?>
                </div>
                <?php if ($standard_finish_lead ): ?>
                <div class="finish-cnt" data-aos="fade-up">
                    <?php echo $standard_finish_lead  ?>
                </div>
                <?php endif ?>
            </div>
        <?php endif ?>
        <?php if ( have_rows( 'standard_finish' ) ) : ?>
            <ul>
                <?php while ( have_rows( 'standard_finish' ) ) : the_row();
                $standard_finish_tekst_left = get_sub_field('standard_finish_tekst_left') ?>
                    <li class="row">
                        <div class="col cnt full">
                            <div class="txt" data-aos="fade-up">
                                <?php $title = get_sub_field( 'standard_finish_title' ); ?>
                                <?php if ($title): ?>
                                    <div class="title-middle">
                                        <?php echo $title;?>
                                    </div>
                                <?php endif ?>
                                <?php the_sub_field( 'standard_finish_tekst' ); ?>
                            </div>
                        </div>
                        <div class="col image">
                            <div class="swiper gallBox">
                                <?php if ( have_rows( 'standard_finish_gallery' ) ) : ?>
                                <div class="swiper-wrapper">
                                <?php while ( have_rows( 'standard_finish_gallery' ) ) : the_row(); ?>
                                    <div class="swiper-slide">
                                    <?php $gallery = get_sub_field( 'standard_finish_gallery_img' ); ?>
                                    <?php $size = 'image960'; ?>
                                    <?php $thumb = $gallery['sizes'][ $size ]; ?>
                                    
                                    <?php if ( $gallery ) : ?>
                                        <img width="960" height="723" data-src="<?php echo esc_url($thumb); ?>" class="swiper-lazy" />
                                        <div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div>
                                    <?php endif; ?>
                                    </div>
                                <?php endwhile; ?>
                                </div>
                                <?php endif; ?>
                                <div class="swiper-pagination"></div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>                            
                        </div>

                        <?php if ($standard_finish_tekst_left): ?>
                        <div class="row-info">
                            <h3 class="full" data-aos="fade-up">
                                <?php echo _e('Elementy wyposażenia','rg');?>
                            </h3>
                            <div class="row-info-container full" data-aos="fade-up">
                                <div class="row-info-col">
                                    <?php echo $standard_finish_tekst_left ?>
                                </div>
                                <div class="row-info-col">
                                   <?php the_sub_field( 'standard_finish_tekst_right' ); ?>
                                </div>    
                            </div>
                            <?php $standard_finish_tekst_info = get_sub_field('standard_finish_tekst_info');
                            if ($standard_finish_tekst_info) :?>
                                <div class="row-info-small container" data-aos="fade-up">
                                    <?php echo $standard_finish_tekst_info ?>
                                </div>
                            <?php endif;?>
                            
                        </div>    
                        <?php endif ?>
                        
                    </li>                
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>