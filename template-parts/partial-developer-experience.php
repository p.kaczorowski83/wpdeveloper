<?php
/**
 * ===============================
 * PARTIAL BO 50/50.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
$developer_experience = get_field( 'developer_experience' );
$developer_experience_left_col = get_field( 'developer_experience_left_col' );
$developer_experience_right_col = get_field( 'developer_experience_right_col' );
?>

<div class="blue-box">

    <div class="blue-box-header">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/svg/img-bg-blue-box.svg" alt="">
    </div>

    <div class="container">
        <?php if ($developer_experience): ?>
            <h3 class="typo1" data-aos="fade-up">
                <?php echo $developer_experience; ?>
            </h3>
        <?php endif ?>

        <?php if ($developer_experience_left_col and $developer_experience_right_col): ?>
            <div class="row">
                <div class="col" data-aos="fade-left">
                    <?php echo $developer_experience_left_col; ?>
                </div>
                <div class="col" data-aos="fade-right">
                    <?php echo $developer_experience_right_col; ?>
                </div>
            </div>
        <?php elseif( $developer_experience_left_col ) :?>
        <div class="text-center" data-aos="fade-up">
            <?php echo $developer_experience_left_col; ?>
        </div>  
        <?php elseif( $developer_experience_right_col ) :?>
        <div class="text-center" data-aos="fade-up">
            <?php echo $developer_experience_right_col; ?>
        </div>           
        <?php endif ?>
    </div>

</div>