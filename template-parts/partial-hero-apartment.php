<?php
/**
 * ===============================
 * HERO.PHP
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
$hero_img = get_field( 'hero_img' );
$hero_title = get_field( 'hero_title' );
$size = 'full'; 
$hero_subtytul = get_field( 'hero_subtytul' );
$hero_img_rwd = get_field('hero_img_rwd')
?>

<?php if ( $hero_img ) : ?>
<div class="hero hero-apartment">

    <div class="hero-bg"></div>

	<div class="container">
		<h1>
            <?php if ($hero_title): ?>
                <?php echo $hero_title ?>
            <?php else: ?>
                <?php the_title();?>
            <?php endif ?>			
		</h1>
        <?php if ($hero_subtytul): ?>
            <p><?php echo $hero_subtytul; ?></p>
        <?php endif ?>
	</div>

    <?php if ( wp_is_mobile() ): ?>
        <?php echo wp_get_attachment_image( $hero_img_rwd , $size, false, [
        'class' => 'lazyload img-fluid',
        'loading' => 'lazy',
        'data-src' => wp_get_attachment_image_url( $hero_img_rwd  , $size ),
        'alt' => get_post_meta( $hero_img_rwd  , '_wp_attachment_image_alt', true),
        ]); 
    ?>
    <?php else: ?>
        <?php echo wp_get_attachment_image( $hero_img, $size, false, [
        'class' => 'lazyload img-fluid',
        'loading' => 'lazy',
        'data-src' => wp_get_attachment_image_url( $hero_img , $size ),
        'alt' => get_post_meta( $hero_img , '_wp_attachment_image_alt', true),
        ]); 
    ?>
    <?php endif;?>
	
</div>
<?php endif; ?>