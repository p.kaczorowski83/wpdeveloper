<?php
/**
 * ===============================
 * STANDARD PACKAGES.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
$standard_packages_subtitle = get_field( 'standard_packages_subtitle' );
$standard_packages_title = get_field( 'standard_packages_title' );
?>

<section class="standard-packages-header">
    <div class="container">
        <?php if ($standard_packages_subtitle): ?>
            <h3 data-aos="fade-up"><?php echo $standard_packages_subtitle; ?></h3>
        <?php endif ?>
        <?php if ($standard_packages_title): ?>
            <h2 data-aos="fade-up"><?php echo $standard_packages_title; ?></h2>
        <?php endif ?>
    </div>
</section>

<section class="standard-packages">
    <div class="menu">
        <ul>
            <?php $ii = 0; while ( have_rows( 'standard_packages' ) ) : the_row(); ?>
                <li id="pack-<?php echo ++$ii;?>">
                    <div class="menu-cnt">
                        <div class="nr">
                        0<?php echo ++$i; ?>
                        </div>
                        <div class="title">
                            <?php echo _e('Pakiet','rg');?>
                        </div>
                        <div class="name">
                            <?php the_sub_field( 'standard_packages_name' ); ?>
                        </div>
                    </div>
                </li>
            <?php endwhile; ?>
        </ul>
    </div>
    <div class="cnt">
        <?php $i = 0; while ( have_rows( 'standard_packages' ) ) : the_row(); ?>
        <div class="cnt-loop" id="pack-<?php echo ++$i;?>C">
            
            <!-- LEAD -->
            <div class="lead">
                <div class="title" data-aos="fade-up">
                    <?php echo _e('Pakiet','rg');?> <?php the_sub_field( 'standard_packages_name' ); ?>
                </div>
                <div class="txt" data-aos="fade-up">
                    <?php the_sub_field( 'standard_packages_info' ); ?>
                </div>
            </div>

            <!-- BOXES -->
            <?php if ( have_rows( 'standard_packages_boxes' ) ) : ?>
            <ul class="cnt-boxes">
                <?php while ( have_rows( 'standard_packages_boxes' ) ) : the_row(); ?>
                    <li>
                        <div class="cnt-boxes-img">
                        <?php $standard_packages_boxes_img = get_sub_field( 'standard_packages_boxes_img' ); ?>
                        <?php $size = 'image600'; ?>
                        <?php if ( $standard_packages_boxes_img ) : ?>
                            <?php echo wp_get_attachment_image( $standard_packages_boxes_img, $size, false, [
                                'class' => 'lazyload img-fluid',
                                'loading' => 'lazy',
                                'data-src' => wp_get_attachment_image_url( $standard_packages_boxes_img , $size ),
                                'alt' => get_post_meta( $standard_packages_boxes_img , '_wp_attachment_image_alt', true),
                                'data-aos' => "fade-up"
                                ]); 
                            ?>
                        <?php endif; ?>
                        </div>
                        <div class="cnt-boxes-txt">
                            <div class="row">
                                <h3 data-aos="fade-up"><?php the_sub_field( 'standard_packages_boxes_title' ); ?></h3>
                                <div data-aos="fade-up">
                                    <?php the_sub_field( 'standard_packages_boxes_cnt' ); ?>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endwhile; ?>
            </ul>
            <?php endif; ?>

            <!-- GALERIA -->
            <div class="cnt-gallery">
                <h3 data-aos="fade-up"><?php echo _e('Galeria wykończeń', 'rg');?></h3>
                <?php $standard_packages_gallery_ids = get_sub_field( 'standard_packages_gallery' ); ?>
                <?php $size = 'image1100'; ?>
                <?php if ( $standard_packages_gallery_ids ) :  ?>
                    <div class="swiper gallBox" data-aos="fade-up">
                        <div class="swiper-wrapper">
                        <?php foreach ( $standard_packages_gallery_ids as $standard_packages_gallery_id ): ?>
                            <div class="swiper-slide">
                                <?php echo wp_get_attachment_image( $standard_packages_gallery_id, $size ); ?>
                            </div>
                        <?php endforeach; ?>
                        </div>
                </div>
                <?php endif; ?>
                <div class="swiper-pagination" style="display: none"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>

            <!-- WYPOSAZENIE -->
            <div class="cnt-equipment">
                <h3><?php the_sub_field( 'standard_packages_equipment_title' ); ?></h3>

                <?php if ( have_rows( 'standard_packages_equipment' ) ) : ?>
                    <ul>
                        <?php while ( have_rows( 'standard_packages_equipment' ) ) : the_row(); ?>
                            <li>
                                <div class="left" data-aos="fade-up">
                                    <?php the_sub_field( 'standard_packages_equipment_name' ); ?>
                                </div>
                                <div class="right" data-aos="fade-up">
                                    <?php the_sub_field( 'standard_packages_equipment_cnt' ); ?>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>

        </div>
        <?php endwhile; ?>
    </div>
</section>