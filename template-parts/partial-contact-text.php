<?php
/**
 * ===============================
 * PARTIAL CONTAC TEXT .PHP - text section 
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<section class="contact-text" id="next">
    <div class="container">
        
        <h2 class="typo1" data-aos="fade-up">
            <?php the_field( 'contact_title' ); ?>
        
        </h2>
        
        <div class="row">
            <div class="col">
                <?php if ( have_rows( 'contact_number' ) ) : ?>
                    <ul class="contact-list">
                    <?php while ( have_rows( 'contact_number' ) ) : the_row(); ?>
                        <li data-aos="fade-up">
                            <?php $contact_number_icon = get_sub_field( 'contact_number_icon' ); ?>
                            <?php $size = 'full'; ?>
                            <?php if ( $contact_number_icon ) : ?>
                                <?php echo wp_get_attachment_image( $contact_number_icon, $size ); ?>
                            <?php endif; ?>
                            <?php the_sub_field( 'contact_number_cnt' ); ?>
                        </li>
                    <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                
            </div>
            <div class="col col-2" data-aos="fade-up">
                <?php the_field( 'contact_number_sale' ); ?>
                <a href="<?php the_field( 'contact_number_maps' ); ?>" target="_blank" class="btn-maps">
                    <?php the_field( 'contact_number_maps_cnt' ); ?>
                </a>
            </div>
        </div>
    </div><!-- end .container -->
</section>