<?php
/**
 * ===============================
 * BANNER QUALITY QUALITY.PHP
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

?>
<div class="banner">
    <?php $about_quality_img = get_field( 'about_quality_img' ); ?>
    <?php $size = 'full'; ?>
    <?php echo wp_get_attachment_image( $about_quality_img, $size, false, [
        'class' => 'lazyload img-fluid',
        'loading' => 'lazy',
        'data-src' => wp_get_attachment_image_url( $about_quality_img , $size ),
        'alt' => get_post_meta( $about_quality_img , '_wp_attachment_image_alt', true),
        ]); 
    ?>
    <?php $about_quality_subtitle = get_field( 'about_quality_subtitle' );
        $about_quality_title = get_field( 'about_quality_title' );
        $about_quality_cnt = get_field( 'about_quality_cnt' ); 
        ?>
    <div class="txt full">
        <?php if ($about_quality_subtitle): ?>
        <div class="subtitle" data-aos="fade-up">
            <?php echo $about_quality_subtitle; ?>
        </div>
        <?php endif ?>
        <?php if ($about_quality_title): ?>
        <div class="title-big" data-aos="fade-up">
            <?php echo $about_quality_title; ?>
        </div>
        <?php endif ?>
        <?php if ($about_quality_cnt): ?>
        <div data-aos="fade-up">
            <?php echo $about_quality_cnt; ?>
        </div>
        <?php endif ?>
    </div>
</div>