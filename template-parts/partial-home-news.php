<?php
/**
 * ===============================
 * PARTIAL HOME NEWS.PHP 
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<div class="home-news">
    <div class="container">
        <div class="row">
            <div class="col" data-aos="fade-up">
                <h3><?php echo _e('Aktualności','rg');?></h3>
            </div>
            <div class="col" data-aos="fade-up">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>aktualnosci/" class="btn-white"><?php echo _e('Pokaż wszystkie','rg');?></a>
            </div>
        </div>
        <ul class="news-list news-list-home">
        <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 3
        );
        $news = new WP_Query( $args );
        if ( $news->have_posts() ) :
        while ($news->have_posts()) : $news->the_post(); 
        ?>
            <li>
                <div class="image" data-aos="fade-up">
                    <?php $news_img = get_field( 'news_img' ); ?>
                    <?php $size = 'image606'; ?>
                    <?php if ( $news_img ) : ?>
                    <?php echo wp_get_attachment_image( $news_img, $size, false, [
                        'class' => 'lazyload img-fluid',
                        'loading' => 'lazy',
                        'data-src' => wp_get_attachment_image_url( $news_img , $size ),
                        'alt' => get_post_meta( $news_img , '_wp_attachment_image_alt', true),
                        'data-aos' => "fade-up"
                        ]); 
                    ?>
                    <?php endif; ?>
                </div>
                <div class="cnt" data-aos="fade-up">
                    <span data-aos="fade-up">
                        <?php the_time( 'F j, Y' ); ?> r.
                    </span>
                    <p data-aos="fade-up">
                        <?php the_field( 'news_cnt' ); ?>
                    </p>
                </div>
            </li>
        <?php endwhile; ?>
        </ul>
        <?php endif; wp_reset_query() ?>
    </div>
</div>