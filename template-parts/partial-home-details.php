<?php
/**
 * ===============================
 * PARTIAL HOME DETAILS.PHP 
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
$home_details_subtitle = get_field( 'home_details_subtitle' );
$home_details_title = get_field('home_details_title');
?>

<div class="home-details">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php if ($home_details_subtitle): ?>
                <h3 data-aos="fade-up">
                    <?php echo $home_details_subtitle ?>
                </h3>
            <?php endif ?>
            <?php if ($home_details_title): ?>
                <h2 data-aos="fade-up">
                    <?php echo $home_details_title ?>
                </h2>
            <?php endif ?>  
            </div>
        </div>

        <!-- CNT -->
        <div class="row text">
            <div class="cnt" data-aos="fade-up">
                <p><?php the_field( 'home_details_cnt' ); ?></p>
                <?php $home_details_link = get_field( 'home_details_link' ); ?>
                <?php if ( $home_details_link ) : ?>
                    <a href="<?php echo esc_url( $home_details_link['url'] ); ?>" class="btn-blue" data-aos="fade-up"><?php echo esc_html( $home_details_link['title'] ); ?></a>
                <?php endif; ?> 
            </div>
            <div class="image" data-aos="fade-up">
                <?php $home_details_img = get_field( 'home_details_img' ); ?>
                <?php $size = 'full'; ?>
                <?php if ( $home_details_img ) : ?>
                    <?php echo wp_get_attachment_image( $home_details_img, $size, false, [
                        'class' => 'lazyload img-fluid',
                        'loading' => 'lazy',
                        'data-src' => wp_get_attachment_image_url( $home_details_img , $size ),
                        'alt' => get_post_meta( $home_details_img , '_wp_attachment_image_alt', true),
                        ]); 
                    ?>
                <?php endif; ?>
            </div>
        </div>
        
    </div>
</div>