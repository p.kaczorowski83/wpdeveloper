<?php
/**
 * ===============================
 * HEADER-LOGO.PHP - logo
 * ===============================
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
?>

<img src="<?php echo get_template_directory_uri(); ?>/assets/svg/logo.svg" alt="<?php bloginfo('name'); ?>" width="218" height="70" class="img-fluid">