<?php
/**
 * ===============================
 * TEMPLATE-PAGE-CONTACT- template for contact page
 * ===============================
 *
 * Template name: Kontakt
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>

    
    <main class="contact">
        <?php 
        // get_template_part( 'template-parts/partial', 'contact-secretariat');
        get_template_part( 'template-parts/partial', 'contact-sale');
        get_template_part( 'template-parts/partial', 'contact-box');
        get_template_part( 'template-parts/partial', 'contact-company');
        get_template_part( 'template-parts/partial', 'contact-form');
        ?>        
    </main>               
     

<?php
get_footer();