<?php
/**
 * ===============================
 * TEMPLATE-PAGE-FINANCING- template for investment page
 * ===============================
 *
 * Template name: Finansowanie
 *
 * @package PG
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
?>

    
    <main>
        <?php get_template_part( 'template-parts/partial', 'financing');?>        
    </main>               
     

<?php
get_footer();