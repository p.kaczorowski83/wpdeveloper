<?php
/**
 * ===============================
 * TEMPLATE-PAGE-APARTMENT- template for apartment page
 * ===============================
 *
 * Template name: Apartamenty - wyszukiwarka
 *
 * @package VELA
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
get_template_part( 'template-parts/partial', 'hero-apartment');
?>

                 
<div id="flat-map">

    <?php if ( !wp_is_mobile() ): ?>

        <?php
        get_template_part( 'template-parts/partial', 'flat-nav');
        get_template_part( 'template-parts/partial', 'flat-build');
        get_template_part( 'template-parts/partial', 'flat-floor');
        ?> 

    <?php endif;?>  

    <?php get_template_part( 'template-parts/partial', 'flat-form');
    get_template_part( 'template-parts/partial', 'flat-search'); ?>

</div>

     

<?php
get_footer();