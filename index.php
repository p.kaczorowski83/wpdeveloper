<?php
/**
 * ===============================
 * INDEX.PHP - The template for displaying content in the home page
 * Template Name: Strona główna
 * ===============================
 *
 * @package RG
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<?php 
    get_template_part('template-parts/partial', 'home-box-foto');
    get_template_part('template-parts/partial', 'home-investments');
    get_template_part('template-parts/partial', 'home-details');
    get_template_part('template-parts/partial', 'home-news');
    get_template_part('template-parts/partial', 'home-banner');
?>

<?php
get_footer();
